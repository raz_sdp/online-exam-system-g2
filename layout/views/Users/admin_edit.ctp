<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Add User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->
                    <?php echo $this->Form->create('User',array('type'=>'file'));?>
                    <div class="box-body">
                        <div class="form-group">

                            <?php  echo $this->Form->input('username',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                        </div>
                        <div class="form-group">

                            <?php  echo $this->Form->input('fullname',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                        </div>
                        <div class="form-group">
                            <?php  echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Email'));?>
                        </div>
                        <div class="form-group">
                            <?php  echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>'Password'));?>
                        </div>
                        <div class="form-group">

                            <?php  echo $this->Form->input('college',array('class'=>'form-control','placeholder'=>'College'));?>
                        </div>
                        <div class="form-group">
                            <?php  echo $this->Form->input('address',array('class'=>'form-control','placeholder'=>'Address'));?>
                        </div>
                        <div class="form-group">

                            <?php  echo $this->Form->input('city',array('class'=>'form-control','placeholder'=>'City'));?>
                        </div>
                        <div class="form-group">

                            <?php  echo $this->Form->input('passing_year',array('class'=>'form-control','placeholder'=>'Passing Year'));?>
                        </div>
                        <div class="form-group">

                            <?php  echo $this->Form->input('mobile',array('class'=>'form-control','placeholder'=>'Mobile'));?>

                        </div>
                        <div class="form-group">

                            <?php  echo $this->Form->input('group',array('class'=>'form-control','placeholder'=>'Group'));?>
                        </div>
                        <div class="form-group">

                            <?php echo $this->Form->input('filename',array('type'=>'file','multiple'=>'multiple'));?>

                    </div>
                        <div class="checkbox">
                            <label>

                                <?php echo $this->Form->input('status');?>

                            </label>
                        </div>

                        <?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
                        <!-- </form>---->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
















<!--<div class="users form">
<?php /*echo $this->Form->create('User'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Edit User'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('fullname');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('college');
		echo $this->Form->input('address');
		echo $this->Form->input('city');
		echo $this->Form->input('passing_year');
		echo $this->Form->input('mobile');
		echo $this->Form->input('pic');
		echo $this->Form->input('group');
		echo $this->Form->input('status');
		echo $this->Form->input('Answer');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); */?></li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Payments'), array('controller' => 'payments', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Payment'), array('controller' => 'payments', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); */?> </li>
	</ul>
</div>
-->