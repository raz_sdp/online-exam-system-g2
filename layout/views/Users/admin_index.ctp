<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Users</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('User', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Group</label>
                                    <div class="col-sm-10">
                                        <?php
                                        echo $this->Form->input('group', array(
                                                'label' => false,
                                                'class' => 'form-control',
                                                'options' => ['' =>'- None -'] + $groups,
                                                'value' => $group
                                            )
                                        ); ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Lecture name"
                                           value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->






                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('fullname'); ?></th>
                                <th><?php echo $this->Paginator->sort('email'); ?></th>
                                <th><?php echo $this->Paginator->sort('password'); ?></th>
                                <th><?php echo $this->Paginator->sort('college'); ?></th>
                                <th><?php echo $this->Paginator->sort('address'); ?></th>
                                <th><?php echo $this->Paginator->sort('city'); ?></th>
                                <th><?php echo $this->Paginator->sort('passing_year'); ?></th>
                                <th><?php echo $this->Paginator->sort('mobile'); ?></th>
                                <th><?php echo $this->Paginator->sort('group'); ?></th>
                                <th><?php echo $this->Paginator->sort('pic'); ?></th>
                                <th><?php echo $this->Paginator->sort('status'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user): ?>
                                <tr>
                                    <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['fullname']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['password']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['college']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['address']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['city']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['passing_year']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['mobile']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['group']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['pic']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['status']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['created']); ?>&nbsp;</td>
                                    <td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
                                    <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>












<!--
<div class="users index">
	<h2><?php /*echo __('Users'); */?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php /*echo $this->Paginator->sort('id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('fullname'); */?></th>
			<th><?php /*echo $this->Paginator->sort('email'); */?></th>
			<th><?php /*echo $this->Paginator->sort('password'); */?></th>
			<th><?php /*echo $this->Paginator->sort('college'); */?></th>
			<th><?php /*echo $this->Paginator->sort('address'); */?></th>
			<th><?php /*echo $this->Paginator->sort('city'); */?></th>
			<th><?php /*echo $this->Paginator->sort('passing_year'); */?></th>
			<th><?php /*echo $this->Paginator->sort('mobile'); */?></th>
			<th><?php /*echo $this->Paginator->sort('group'); */?></th>
            <th><?php /*echo $this->Paginator->sort('pic'); */?></th>
			<th><?php /*echo $this->Paginator->sort('status'); */?></th>
			<th><?php /*echo $this->Paginator->sort('created'); */?></th>
			<th><?php /*echo $this->Paginator->sort('modified'); */?></th>
			<th class="actions"><?php /*echo __('Actions'); */?></th>
	</tr>
	</thead>
	<tbody>
	<?php /*foreach ($users as $user): */?>
	<tr>
		<td><?php /*echo h($user['User']['id']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['fullname']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['email']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['password']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['college']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['address']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['city']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['passing_year']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['mobile']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['group']); */?>&nbsp;</td>
        <td><?php /*echo h($user['User']['pic']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['status']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['created']); */?>&nbsp;</td>
		<td><?php /*echo h($user['User']['modified']); */?>&nbsp;</td>
		<td class="actions">
			<?php /*echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id'])); */?>
			<?php /*echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); */?>
			<?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); */?>
		</td>
	</tr>
<?php /*endforeach; */?>
	</tbody>
	</table>
	<p>
	<?php
/*	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	*/?>	</p>
	<div class="paging">
	<?php
/*		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	*/?>
	</div>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>
		<li><?php /*echo $this->Html->link(__('New User'), array('action' => 'add')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Payments'), array('controller' => 'payments', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Payment'), array('controller' => 'payments', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); */?> </li>
	</ul>
</div>-->
