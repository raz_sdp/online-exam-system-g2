/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : onexam2

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-01-23 01:04:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `answers`
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `answer` text,
  `feedback` text,
  `is_correct` tinyint(3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=466 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES ('17', '3', 'R-CHO ', 'Nomamfdmjkdhnnnnnnnnnnnnnnnnnn', null, '2016-11-30 08:12:52', '2017-12-16 08:45:23');
INSERT INTO `answers` VALUES ('18', '1', 'R-OH', null, '1', '2016-11-30 08:12:52', '2016-11-30 08:12:52');
INSERT INTO `answers` VALUES ('19', '1', 'R-COOR', null, null, '2016-11-30 08:12:52', '2016-11-30 08:12:52');
INSERT INTO `answers` VALUES ('20', '1', 'R-COOH', null, null, '2016-11-30 08:12:52', '2016-11-30 08:12:52');
INSERT INTO `answers` VALUES ('21', '2', 'R-CHO', null, null, '2016-11-30 08:13:20', '2016-11-30 08:13:20');
INSERT INTO `answers` VALUES ('22', '2', 'R-OH', null, '1', '2016-11-30 08:13:20', '2016-11-30 08:13:20');
INSERT INTO `answers` VALUES ('23', '2', 'R-COOR', null, null, '2016-11-30 08:13:20', '2016-11-30 08:13:20');
INSERT INTO `answers` VALUES ('24', '2', 'R-COOH', null, null, '2016-11-30 08:13:20', '2016-11-30 08:13:20');
INSERT INTO `answers` VALUES ('57', '11', 'RNH2', null, null, '2016-11-30 22:56:21', '2016-11-30 22:56:21');
INSERT INTO `answers` VALUES ('58', '11', 'BF3', null, null, '2016-11-30 22:56:21', '2016-11-30 22:56:21');
INSERT INTO `answers` VALUES ('59', '11', 'ROH', null, null, '2016-11-30 22:56:21', '2016-11-30 22:56:21');
INSERT INTO `answers` VALUES ('60', '11', 'RMgX', null, null, '2016-11-30 22:56:21', '2016-11-30 22:56:21');
INSERT INTO `answers` VALUES ('61', '12', 'SP-SP3', null, null, '2016-11-30 23:02:47', '2016-11-30 23:02:47');
INSERT INTO `answers` VALUES ('62', '12', 'SP2-SP3', null, null, '2016-11-30 23:02:47', '2016-11-30 23:02:47');
INSERT INTO `answers` VALUES ('63', '12', 'SP-SP2', null, null, '2016-11-30 23:02:47', '2016-11-30 23:02:47');
INSERT INTO `answers` VALUES ('64', '12', 'SP3-SP3', null, null, '2016-11-30 23:02:47', '2016-11-30 23:02:47');
INSERT INTO `answers` VALUES ('65', '13', 'CH2(OH)CH2(OH)', null, null, '2016-11-30 23:06:34', '2016-11-30 23:06:34');
INSERT INTO `answers` VALUES ('66', '13', '(CHO)2', null, null, '2016-11-30 23:06:34', '2016-11-30 23:06:34');
INSERT INTO `answers` VALUES ('67', '13', 'CH3OH', null, null, '2016-11-30 23:06:34', '2016-11-30 23:06:34');
INSERT INTO `answers` VALUES ('68', '13', 'None', null, null, '2016-11-30 23:06:34', '2016-11-30 23:06:34');
INSERT INTO `answers` VALUES ('109', '17', '<p>HNO<sub>3</sub>&amp; H<sub>2</sub>SO<sub>4</sub></p>\r\n', null, null, '2016-12-05 00:50:38', '2016-12-05 00:50:38');
INSERT INTO `answers` VALUES ('110', '17', '<p>NaNO<sub>3</sub> &amp; HNO<sub>3</sub></p>\r\n', null, null, '2016-12-05 00:50:38', '2016-12-05 00:50:38');
INSERT INTO `answers` VALUES ('111', '17', '<p>NaNO<sub>3</sub>&amp;HCl</p>\r\n', null, null, '2016-12-05 00:50:38', '2016-12-05 00:50:38');
INSERT INTO `answers` VALUES ('112', '17', '<p>Dil HNO<sub>3</sub></p>\r\n', null, null, '2016-12-05 00:50:38', '2016-12-05 00:50:38');
INSERT INTO `answers` VALUES ('117', '18', '<p>a) CH<sub>3</sub>(CH<sub>3</sub>)=C(CH<sub>3</sub>)-CH<sub>3</sub></p>\r\n', null, null, '2016-12-05 00:54:34', '2016-12-05 00:54:34');
INSERT INTO `answers` VALUES ('118', '18', '<p>CH<sub>3</sub>CH=CH-CH(CH<sub>3</sub>)-CH<sub>3</sub></p>\r\n', null, null, '2016-12-05 00:54:34', '2016-12-05 00:54:34');
INSERT INTO `answers` VALUES ('119', '18', '<p>CH<sub>3</sub>CH<sub>2</sub>CH=CH-CH<sub>2</sub>-CH<sub>3</sub></p>\r\n', null, null, '2016-12-05 00:54:34', '2016-12-05 00:54:34');
INSERT INTO `answers` VALUES ('120', '18', '<p>CH<sub>3</sub>=CH(CH<sub>3</sub>&not;)-CH(CH<sub>3</sub>)-CH<sub>3</sub></p>\r\n', null, null, '2016-12-05 00:54:34', '2016-12-05 00:54:34');
INSERT INTO `answers` VALUES ('129', '20', '<p>&ndash;OH</p>\r\n', null, null, '2016-12-05 01:04:47', '2016-12-05 01:04:47');
INSERT INTO `answers` VALUES ('130', '20', '<p>&ndash;NO<sub>2</sub></p>\r\n', null, null, '2016-12-05 01:04:47', '2016-12-05 01:04:47');
INSERT INTO `answers` VALUES ('131', '20', '<p>&ndash;NH<sub>2</sub></p>\r\n', null, null, '2016-12-05 01:04:47', '2016-12-05 01:04:47');
INSERT INTO `answers` VALUES ('132', '20', '<p>&ndash;CH<sub>3</sub></p>\r\n', null, null, '2016-12-05 01:04:47', '2016-12-05 01:04:47');
INSERT INTO `answers` VALUES ('133', '21', 'Chlorobenzene ', null, null, '2016-12-05 01:07:01', '2016-12-05 01:07:01');
INSERT INTO `answers` VALUES ('134', '21', 'O-Chlorotoluene ', null, null, '2016-12-05 01:07:01', '2016-12-05 01:07:01');
INSERT INTO `answers` VALUES ('135', '21', 'P-chlorotoluene ', null, null, '2016-12-05 01:07:01', '2016-12-05 01:07:01');
INSERT INTO `answers` VALUES ('136', '21', 'Benzochloride ', null, null, '2016-12-05 01:07:01', '2016-12-05 01:07:01');
INSERT INTO `answers` VALUES ('145', '24', 'Chlorobenzene ', null, null, '2016-12-05 09:28:28', '2016-12-05 09:28:28');
INSERT INTO `answers` VALUES ('146', '24', 'O-Chlorotoluene ', null, null, '2016-12-05 09:28:28', '2016-12-05 09:28:28');
INSERT INTO `answers` VALUES ('147', '24', 'P-chlorotoluene ', null, null, '2016-12-05 09:28:28', '2016-12-05 09:28:28');
INSERT INTO `answers` VALUES ('148', '24', 'Benzochloride ', null, null, '2016-12-05 09:28:28', '2016-12-05 09:28:28');
INSERT INTO `answers` VALUES ('153', '25', '<p>n(CF<sub>2</sub>&not;-CF<sub>2</sub>)</p>\r\n', null, null, '2016-12-05 09:31:35', '2016-12-05 09:31:35');
INSERT INTO `answers` VALUES ('154', '25', '<p>(-CF<sub>2</sub>-CF<sub>2</sub>-)n</p>\r\n', null, null, '2016-12-05 09:31:35', '2016-12-05 09:31:35');
INSERT INTO `answers` VALUES ('155', '25', '<p>(C<sub>2</sub>F<sub>4</sub>)n</p>\r\n', null, null, '2016-12-05 09:31:35', '2016-12-05 09:31:35');
INSERT INTO `answers` VALUES ('156', '25', '<p>both b+c</p>\r\n', null, null, '2016-12-05 09:31:35', '2016-12-05 09:31:35');
INSERT INTO `answers` VALUES ('165', '27', 'Ozonolysis ', null, null, '2016-12-05 09:35:34', '2016-12-05 09:35:34');
INSERT INTO `answers` VALUES ('166', '27', 'Salphonation', null, null, '2016-12-05 09:35:34', '2016-12-05 09:35:34');
INSERT INTO `answers` VALUES ('167', '27', 'Nitration ', null, null, '2016-12-05 09:35:34', '2016-12-05 09:35:34');
INSERT INTO `answers` VALUES ('168', '27', 'Halogenations', null, null, '2016-12-05 09:35:34', '2016-12-05 09:35:34');
INSERT INTO `answers` VALUES ('173', '28', '<p>CH<sub>3</sub>CH<sub>2</sub>CH<sub>2</sub>OH</p>\r\n', null, null, '2016-12-05 09:38:25', '2016-12-05 09:38:25');
INSERT INTO `answers` VALUES ('174', '28', '<p>CH<sub>3</sub>CHOHCH<sub>3</sub></p>\r\n', null, null, '2016-12-05 09:38:25', '2016-12-05 09:38:25');
INSERT INTO `answers` VALUES ('175', '28', '<p>CH<sub>3</sub>CH<sub>2</sub>OH</p>\r\n', null, null, '2016-12-05 09:38:25', '2016-12-05 09:38:25');
INSERT INTO `answers` VALUES ('176', '28', '<p>None</p>\r\n', null, null, '2016-12-05 09:38:25', '2016-12-05 09:38:25');
INSERT INTO `answers` VALUES ('177', '26', '<p>Light</p>\r\n', null, null, '2016-12-05 09:41:13', '2016-12-05 09:41:13');
INSERT INTO `answers` VALUES ('178', '26', '<p>AlCl<sub>3</sub>/HCl</p>\r\n', null, null, '2016-12-05 09:41:13', '2016-12-05 09:41:13');
INSERT INTO `answers` VALUES ('179', '26', '<p>Al<sub>2</sub>O<sub>3</sub></p>\r\n', null, null, '2016-12-05 09:41:13', '2016-12-05 09:41:13');
INSERT INTO `answers` VALUES ('180', '26', '<p>Cr<sub>2</sub>O<sub>3</sub></p>\r\n', null, null, '2016-12-05 09:41:13', '2016-12-05 09:41:13');
INSERT INTO `answers` VALUES ('185', '29', '<p>-CHO</p>\r\n', null, null, '2016-12-05 09:43:15', '2016-12-05 09:43:15');
INSERT INTO `answers` VALUES ('186', '29', '<p>-OH</p>\r\n', null, null, '2016-12-05 09:43:15', '2016-12-05 09:43:15');
INSERT INTO `answers` VALUES ('187', '29', '<p>-(CO)<sub>2</sub>O</p>\r\n', null, null, '2016-12-05 09:43:15', '2016-12-05 09:43:15');
INSERT INTO `answers` VALUES ('188', '29', '<p>&ndash;C=O</p>\r\n', null, null, '2016-12-05 09:43:15', '2016-12-05 09:43:15');
INSERT INTO `answers` VALUES ('189', '30', '??????????????? ', null, null, '2016-12-05 09:44:25', '2016-12-05 09:44:25');
INSERT INTO `answers` VALUES ('190', '30', '???????  		', null, null, '2016-12-05 09:44:25', '2016-12-05 09:44:25');
INSERT INTO `answers` VALUES ('191', '30', '????????	', null, null, '2016-12-05 09:44:25', '2016-12-05 09:44:25');
INSERT INTO `answers` VALUES ('192', '30', '?????? ', null, null, '2016-12-05 09:44:25', '2016-12-05 09:44:25');
INSERT INTO `answers` VALUES ('193', '31', 'Nitrobenzene', null, null, '2016-12-05 09:45:41', '2016-12-05 09:45:41');
INSERT INTO `answers` VALUES ('194', '31', 'Anilene ', null, null, '2016-12-05 09:45:41', '2016-12-05 09:45:41');
INSERT INTO `answers` VALUES ('195', '31', 'Chlorobenzene ', null, null, '2016-12-05 09:45:41', '2016-12-05 09:45:41');
INSERT INTO `answers` VALUES ('196', '31', 'Amino benzene', null, null, '2016-12-05 09:45:41', '2016-12-05 09:45:41');
INSERT INTO `answers` VALUES ('205', '33', '<p>C<sub>6</sub>H<sub>6</sub></p>\r\n', null, null, '2016-12-05 09:51:23', '2016-12-05 09:51:23');
INSERT INTO `answers` VALUES ('206', '33', '<p>C<sub>6</sub>H<sub>5</sub>CHO</p>\r\n', null, null, '2016-12-05 09:51:23', '2016-12-05 09:51:23');
INSERT INTO `answers` VALUES ('207', '33', '<p>NH<sub>4</sub></p>\r\n', null, null, '2016-12-05 09:51:23', '2016-12-05 09:51:23');
INSERT INTO `answers` VALUES ('208', '33', '<p>CH<sub>4</sub></p>\r\n', null, null, '2016-12-05 09:51:23', '2016-12-05 09:51:23');
INSERT INTO `answers` VALUES ('209', '15', '<p>Acetophenone</p>\r\n', null, null, '2016-12-05 09:54:23', '2016-12-05 09:54:23');
INSERT INTO `answers` VALUES ('210', '15', '<p>Acetone</p>\r\n', null, null, '2016-12-05 09:54:23', '2016-12-05 09:54:23');
INSERT INTO `answers` VALUES ('211', '15', '<p>Phenol</p>\r\n', null, null, '2016-12-05 09:54:23', '2016-12-05 09:54:23');
INSERT INTO `answers` VALUES ('212', '15', '<p>Benzyl Chloride</p>\r\n', null, null, '2016-12-05 09:54:23', '2016-12-05 09:54:23');
INSERT INTO `answers` VALUES ('217', '32', '<p><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">?????&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>\r\n', null, null, '2016-12-05 10:16:13', '2016-12-05 10:16:13');
INSERT INTO `answers` VALUES ('218', '32', '<p><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">???????? </span></p>\r\n', null, null, '2016-12-05 10:16:13', '2016-12-05 10:16:13');
INSERT INTO `answers` VALUES ('219', '32', '<p><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">??????</span></p>\r\n', null, null, '2016-12-05 10:16:13', '2016-12-05 10:16:13');
INSERT INTO `answers` VALUES ('220', '32', '<p>None</p>\r\n', null, null, '2016-12-05 10:16:13', '2016-12-05 10:16:13');
INSERT INTO `answers` VALUES ('221', '23', '<p>Saturated &ndash;H-carbon</p>\r\n', null, null, '2016-12-05 10:17:39', '2016-12-05 10:17:39');
INSERT INTO `answers` VALUES ('222', '23', '<p>non-saturatedHC</p>\r\n', null, null, '2016-12-05 10:17:39', '2016-12-05 10:17:39');
INSERT INTO `answers` VALUES ('223', '23', '<p>both</p>\r\n', null, null, '2016-12-05 10:17:40', '2016-12-05 10:17:40');
INSERT INTO `answers` VALUES ('224', '23', '<p>none</p>\r\n', null, null, '2016-12-05 10:17:40', '2016-12-05 10:17:40');
INSERT INTO `answers` VALUES ('241', '34', 'a)	R-OH     ', null, null, '2016-12-11 23:25:59', '2016-12-11 23:25:59');
INSERT INTO `answers` VALUES ('242', '34', 'R-O-R  ', null, null, '2016-12-11 23:25:59', '2016-12-11 23:25:59');
INSERT INTO `answers` VALUES ('243', '34', 'R-CHO  ', null, null, '2016-12-11 23:25:59', '2016-12-11 23:25:59');
INSERT INTO `answers` VALUES ('244', '34', 'R-COOH', null, null, '2016-12-11 23:25:59', '2016-12-11 23:25:59');
INSERT INTO `answers` VALUES ('249', '35', '<p>65%C<sub>2</sub>H<sub>5</sub>OH+5%H<sub>2</sub>O</p>\r\n', null, null, '2016-12-11 23:30:30', '2016-12-11 23:30:30');
INSERT INTO `answers` VALUES ('250', '35', '<p>95.6%CH<sub>3</sub>OH+4.4%H<sub>2</sub>O</p>\r\n', null, null, '2016-12-11 23:30:30', '2016-12-11 23:30:30');
INSERT INTO `answers` VALUES ('251', '35', '<p>70%CH<sub>3</sub>OH+30%H<sub>2</sub>O</p>\r\n', null, null, '2016-12-11 23:30:30', '2016-12-11 23:30:30');
INSERT INTO `answers` VALUES ('252', '35', '<p>None</p>\r\n', null, null, '2016-12-11 23:30:30', '2016-12-11 23:30:30');
INSERT INTO `answers` VALUES ('253', '36', 'Alkane    ', null, null, '2016-12-11 23:31:55', '2016-12-11 23:31:55');
INSERT INTO `answers` VALUES ('254', '36', 'Ether  ', null, null, '2016-12-11 23:31:55', '2016-12-11 23:31:55');
INSERT INTO `answers` VALUES ('255', '36', 'Ester  ', null, null, '2016-12-11 23:31:55', '2016-12-11 23:31:55');
INSERT INTO `answers` VALUES ('256', '36', 'Aldehyde ', null, null, '2016-12-11 23:31:55', '2016-12-11 23:31:55');
INSERT INTO `answers` VALUES ('257', '37', 'Nitrobenzene    ', null, null, '2016-12-11 23:33:17', '2016-12-11 23:33:17');
INSERT INTO `answers` VALUES ('258', '37', 'Anilene  ', null, null, '2016-12-11 23:33:17', '2016-12-11 23:33:17');
INSERT INTO `answers` VALUES ('259', '37', 'Chlorobenzene  ', null, null, '2016-12-11 23:33:17', '2016-12-11 23:33:17');
INSERT INTO `answers` VALUES ('260', '37', 'Amino benzene', null, null, '2016-12-11 23:33:17', '2016-12-11 23:33:17');
INSERT INTO `answers` VALUES ('265', '38', '<p>CH<sub>3</sub>Cl , HCl</p>\r\n', null, null, '2016-12-11 23:36:35', '2016-12-11 23:36:35');
INSERT INTO `answers` VALUES ('266', '38', '<p>C, HCl</p>\r\n', null, null, '2016-12-11 23:36:35', '2016-12-11 23:36:35');
INSERT INTO `answers` VALUES ('267', '38', '<p>CCl<sub>4</sub>, HCl</p>\r\n', null, null, '2016-12-11 23:36:35', '2016-12-11 23:36:35');
INSERT INTO `answers` VALUES ('268', '38', '<p>CHCl<sub>3</sub> , HCl</p>\r\n', null, null, '2016-12-11 23:36:35', '2016-12-11 23:36:35');
INSERT INTO `answers` VALUES ('269', '39', 'Iso-cyanide    ', null, null, '2016-12-11 23:37:49', '2016-12-11 23:37:49');
INSERT INTO `answers` VALUES ('270', '39', 'Chlorination  ', null, null, '2016-12-11 23:37:49', '2016-12-11 23:37:49');
INSERT INTO `answers` VALUES ('271', '39', 'Redox reaction  ', null, null, '2016-12-11 23:37:49', '2016-12-11 23:37:49');
INSERT INTO `answers` VALUES ('272', '39', 'Wurtz reaction', null, null, '2016-12-11 23:37:49', '2016-12-11 23:37:49');
INSERT INTO `answers` VALUES ('277', '40', '<p>1<sup>0</sup> Alcohole</p>\r\n', null, null, '2016-12-11 23:40:03', '2016-12-11 23:40:03');
INSERT INTO `answers` VALUES ('278', '40', '<p>2<sup>0</sup> Alcohole</p>\r\n', null, null, '2016-12-11 23:40:03', '2016-12-11 23:40:03');
INSERT INTO `answers` VALUES ('279', '40', '<p>3<sup>0</sup> Alcohole</p>\r\n', null, null, '2016-12-11 23:40:03', '2016-12-11 23:40:03');
INSERT INTO `answers` VALUES ('280', '40', '<p>none</p>\r\n', null, null, '2016-12-11 23:40:03', '2016-12-11 23:40:03');
INSERT INTO `answers` VALUES ('281', '41', '???????????', null, null, '2016-12-11 23:48:51', '2016-12-11 23:48:51');
INSERT INTO `answers` VALUES ('282', '41', '??????', null, null, '2016-12-11 23:48:51', '2016-12-11 23:48:51');
INSERT INTO `answers` VALUES ('283', '41', '???????', null, null, '2016-12-11 23:48:51', '2016-12-11 23:48:51');
INSERT INTO `answers` VALUES ('284', '41', '?????????\r\n', null, null, '2016-12-11 23:48:51', '2016-12-11 23:48:51');
INSERT INTO `answers` VALUES ('289', '42', '<p>CH<sub>3</sub>CH(OH)CH<sub>3</sub></p>\r\n', null, null, '2016-12-11 23:52:26', '2016-12-11 23:52:26');
INSERT INTO `answers` VALUES ('290', '42', '<p>CH<sub>3</sub>OH</p>\r\n', null, null, '2016-12-11 23:52:26', '2016-12-11 23:52:26');
INSERT INTO `answers` VALUES ('291', '42', '<p>CH<sub>3</sub>COCH<sub>3</sub></p>\r\n', null, null, '2016-12-11 23:52:26', '2016-12-11 23:52:26');
INSERT INTO `answers` VALUES ('292', '42', '<p>CH<sub>3</sub>C(CH<sub>3</sub>)<sub>2</sub>OH</p>\r\n', null, null, '2016-12-11 23:52:26', '2016-12-11 23:52:26');
INSERT INTO `answers` VALUES ('297', '43', '<p>CH<sub>3</sub>CH<sub>2</sub>OH</p>\r\n', null, null, '2016-12-11 23:55:30', '2016-12-11 23:55:30');
INSERT INTO `answers` VALUES ('298', '43', '<p>CH<sub>3</sub>OH</p>\r\n', null, null, '2016-12-11 23:55:30', '2016-12-11 23:55:30');
INSERT INTO `answers` VALUES ('299', '43', '<p>a+b</p>\r\n', null, null, '2016-12-11 23:55:30', '2016-12-11 23:55:30');
INSERT INTO `answers` VALUES ('300', '43', '<p>CH(CH<sub>3</sub>)<sub>2</sub>OH</p>\r\n', null, null, '2016-12-11 23:55:30', '2016-12-11 23:55:30');
INSERT INTO `answers` VALUES ('301', '44', 'Ethyle chloride      ', null, null, '2016-12-11 23:56:50', '2016-12-11 23:56:50');
INSERT INTO `answers` VALUES ('302', '44', 'Ethane ', null, null, '2016-12-11 23:56:50', '2016-12-11 23:56:50');
INSERT INTO `answers` VALUES ('303', '44', 'Ether    ', null, null, '2016-12-11 23:56:50', '2016-12-11 23:56:50');
INSERT INTO `answers` VALUES ('304', '44', 'Acetyle chloride ', null, null, '2016-12-11 23:56:50', '2016-12-11 23:56:50');
INSERT INTO `answers` VALUES ('309', '45', '<p>NaNO<sub>2</sub> , H<sub>2</sub>SO<sub>4</sub></p>\r\n', null, null, '2016-12-12 00:09:06', '2016-12-12 00:09:06');
INSERT INTO `answers` VALUES ('310', '45', '<p>??? NaOH</p>\r\n', null, null, '2016-12-12 00:09:06', '2016-12-12 00:09:06');
INSERT INTO `answers` VALUES ('311', '45', '<p>?????? AlCl3</p>\r\n', null, null, '2016-12-12 00:09:06', '2016-12-12 00:09:06');
INSERT INTO `answers` VALUES ('312', '45', '<p>1250C/4-7atm</p>\r\n', null, null, '2016-12-12 00:09:06', '2016-12-12 00:09:06');
INSERT INTO `answers` VALUES ('317', '47', '??????                                 ', null, null, '2016-12-12 00:11:08', '2016-12-12 00:11:08');
INSERT INTO `answers` VALUES ('318', '47', '??????', null, null, '2016-12-12 00:11:08', '2016-12-12 00:11:08');
INSERT INTO `answers` VALUES ('319', '47', '????', null, null, '2016-12-12 00:11:08', '2016-12-12 00:11:08');
INSERT INTO `answers` VALUES ('320', '47', '?????? ????', null, null, '2016-12-12 00:11:08', '2016-12-12 00:11:08');
INSERT INTO `answers` VALUES ('325', '48', '<p>1<sup>0</sup> &lt; 2<sup>0</sup>&lt; 3<sup>0</sup></p>\r\n', null, null, '2016-12-12 00:14:41', '2016-12-12 00:14:41');
INSERT INTO `answers` VALUES ('326', '48', '<p>2<sup>0</sup>&lt; 3<sup>0</sup> &lt; 1<sup>0</sup></p>\r\n', null, null, '2016-12-12 00:14:41', '2016-12-12 00:14:41');
INSERT INTO `answers` VALUES ('327', '48', '<p>3<sup>0</sup> &lt;1<sup>0</sup> &lt; 2<sup>0</sup></p>\r\n', null, null, '2016-12-12 00:14:41', '2016-12-12 00:14:41');
INSERT INTO `answers` VALUES ('328', '48', '<p>1<sup>0</sup> &gt; 2<sup>0</sup>&gt; 3<sup>0</sup></p>\r\n', null, null, '2016-12-12 00:14:41', '2016-12-12 00:14:41');
INSERT INTO `answers` VALUES ('333', '49', '<p>40% Glycole+60%H<sub>2</sub>O</p>\r\n', null, null, '2016-12-12 00:17:34', '2016-12-12 00:17:34');
INSERT INTO `answers` VALUES ('334', '49', '<p>60% Glycole+40%H<sub>2</sub>O</p>\r\n', null, null, '2016-12-12 00:17:34', '2016-12-12 00:17:34');
INSERT INTO `answers` VALUES ('335', '49', '<p>80% Glycole+20%H<sub>2</sub>O</p>\r\n', null, null, '2016-12-12 00:17:34', '2016-12-12 00:17:34');
INSERT INTO `answers` VALUES ('336', '49', '<p>None</p>\r\n', null, null, '2016-12-12 00:17:34', '2016-12-12 00:17:34');
INSERT INTO `answers` VALUES ('337', '50', '2', null, null, '2016-12-12 00:18:22', '2016-12-12 00:18:22');
INSERT INTO `answers` VALUES ('338', '50', '10', null, null, '2016-12-12 00:18:22', '2016-12-12 00:18:22');
INSERT INTO `answers` VALUES ('339', '50', '12', null, null, '2016-12-12 00:18:22', '2016-12-12 00:18:22');
INSERT INTO `answers` VALUES ('340', '50', '22', null, null, '2016-12-12 00:18:22', '2016-12-12 00:18:22');
INSERT INTO `answers` VALUES ('349', '51', '<p>???????????? ????</p>\r\n', null, null, '2016-12-12 00:41:36', '2016-12-12 00:41:36');
INSERT INTO `answers` VALUES ('350', '51', '<p>?????????? ???????????</p>\r\n', null, null, '2016-12-12 00:41:36', '2016-12-12 00:41:36');
INSERT INTO `answers` VALUES ('351', '51', '<p>Both</p>\r\n', null, null, '2016-12-12 00:41:36', '2016-12-12 00:41:36');
INSERT INTO `answers` VALUES ('352', '51', '<p>None</p>\r\n', null, null, '2016-12-12 00:41:36', '2016-12-12 00:41:36');
INSERT INTO `answers` VALUES ('357', '3', '<p>2,2,4-Trimethypentane</p>\r\n', null, null, '2017-02-10 03:17:07', '2017-02-10 03:17:07');
INSERT INTO `answers` VALUES ('358', '3', '<p>Isopentane</p>\r\n', null, null, '2017-02-10 03:17:09', '2017-02-10 03:17:09');
INSERT INTO `answers` VALUES ('359', '3', '<p>Neooctane</p>\r\n', null, null, '2017-02-10 03:17:09', '2017-02-10 03:17:09');
INSERT INTO `answers` VALUES ('360', '3', '<p>2,4,4- Trimethypentane</p>\r\n', null, '1', '2017-02-10 03:17:09', '2017-02-10 03:17:09');
INSERT INTO `answers` VALUES ('361', '4', '<p>CH<sub>3</sub>CH=CH<sub>2</sub></p>\r\n', null, null, '2017-03-15 06:24:47', '2017-03-15 06:24:47');
INSERT INTO `answers` VALUES ('362', '4', '<p>ClCH=CHCl</p>\r\n', null, null, '2017-03-15 06:24:47', '2017-03-15 06:24:47');
INSERT INTO `answers` VALUES ('363', '4', '<p>CH2=CH2</p>\r\n', null, '1', '2017-03-15 06:24:47', '2017-03-15 06:24:47');
INSERT INTO `answers` VALUES ('364', '4', '<p>CH<sub>3</sub>CH<sub>3</sub></p>\r\n', null, null, '2017-03-15 06:24:47', '2017-03-15 06:24:47');
INSERT INTO `answers` VALUES ('365', '5', '<p>2,3-dimethylbutane</p>\r\n', null, '1', '2017-03-15 06:25:46', '2017-03-15 06:25:46');
INSERT INTO `answers` VALUES ('366', '5', '<p>2,2-methylbutane</p>\r\n', null, null, '2017-03-15 06:25:46', '2017-03-15 06:25:46');
INSERT INTO `answers` VALUES ('367', '5', '<p>2-methylpentane</p>\r\n', null, null, '2017-03-15 06:25:46', '2017-03-15 06:25:46');
INSERT INTO `answers` VALUES ('368', '5', '<p>2,2-dimethylpropen</p>\r\n', null, null, '2017-03-15 06:25:46', '2017-03-15 06:25:46');
INSERT INTO `answers` VALUES ('369', '6', '<p>CH<span style=\"font-size:10.8333px\">4</span></p>\r\n', null, null, '2017-03-15 06:26:01', '2017-03-15 06:26:01');
INSERT INTO `answers` VALUES ('370', '6', '<p>CH<sub>2</sub>&nbsp;= CH<sub>2</sub></p>\r\n', null, '1', '2017-03-15 06:26:01', '2017-03-15 06:26:01');
INSERT INTO `answers` VALUES ('371', '6', '<p>CH=CH</p>\r\n', null, null, '2017-03-15 06:26:01', '2017-03-15 06:26:01');
INSERT INTO `answers` VALUES ('372', '6', '<p>None</p>\r\n', null, null, '2017-03-15 06:26:01', '2017-03-15 06:26:01');
INSERT INTO `answers` VALUES ('377', '8', '<p>Polarimetre</p>\r\n', null, '1', '2017-03-15 06:26:50', '2017-03-15 06:26:50');
INSERT INTO `answers` VALUES ('378', '8', '<p>Barometre</p>\r\n', null, null, '2017-03-15 06:26:50', '2017-03-15 06:26:50');
INSERT INTO `answers` VALUES ('379', '8', '<p>Manometre</p>\r\n', null, null, '2017-03-15 06:26:50', '2017-03-15 06:26:50');
INSERT INTO `answers` VALUES ('380', '8', '<p>Tacometre</p>\r\n', null, null, '2017-03-15 06:26:50', '2017-03-15 06:26:50');
INSERT INTO `answers` VALUES ('381', '9', '<p>H<sub>3</sub>C</p>\r\n', null, null, '2017-03-15 06:27:06', '2017-03-15 06:27:06');
INSERT INTO `answers` VALUES ('382', '9', '<p>RH<sub>2</sub>C</p>\r\n', null, null, '2017-03-15 06:27:06', '2017-03-15 06:27:06');
INSERT INTO `answers` VALUES ('383', '9', '<p>R<sub>2</sub>HC</p>\r\n', null, null, '2017-03-15 06:27:06', '2017-03-15 06:27:06');
INSERT INTO `answers` VALUES ('384', '9', '<p>R<sub>2</sub>C</p>\r\n', null, '1', '2017-03-15 06:27:06', '2017-03-15 06:27:06');
INSERT INTO `answers` VALUES ('385', '10', '<p>+2.24</p>\r\n', null, '1', '2017-03-15 06:27:23', '2017-03-15 06:27:23');
INSERT INTO `answers` VALUES ('386', '10', '<p>-2.24</p>\r\n', null, null, '2017-03-15 06:27:23', '2017-03-15 06:27:23');
INSERT INTO `answers` VALUES ('387', '10', '<p>+4.48</p>\r\n', null, null, '2017-03-15 06:27:23', '2017-03-15 06:27:23');
INSERT INTO `answers` VALUES ('388', '10', '<p>-4.28</p>\r\n', null, null, '2017-03-15 06:27:23', '2017-03-15 06:27:23');
INSERT INTO `answers` VALUES ('389', '52', 'dfgfdg\r\n', null, null, '2017-12-16 05:57:41', '2017-12-16 05:57:41');
INSERT INTO `answers` VALUES ('390', '52', 'sda\r\n', null, '1', '2017-12-16 05:57:41', '2017-12-16 05:57:41');
INSERT INTO `answers` VALUES ('391', '52', 'sdsa\r\n', null, null, '2017-12-16 05:57:42', '2017-12-16 05:57:42');
INSERT INTO `answers` VALUES ('392', '52', 'sdsa\r\n', null, null, '2017-12-16 05:57:42', '2017-12-16 05:57:42');
INSERT INTO `answers` VALUES ('393', '53', 'dfgfdg\r\n', null, null, '2017-12-16 05:58:34', '2017-12-16 05:58:34');
INSERT INTO `answers` VALUES ('394', '53', 'sda\r\n', null, '1', '2017-12-16 05:58:34', '2017-12-16 05:58:34');
INSERT INTO `answers` VALUES ('395', '53', 'sdsa\r\n', null, null, '2017-12-16 05:58:34', '2017-12-16 05:58:34');
INSERT INTO `answers` VALUES ('396', '53', 'sdsa\r\n', null, null, '2017-12-16 05:58:34', '2017-12-16 05:58:34');
INSERT INTO `answers` VALUES ('417', '16', 'Grignrad reaction\r\n', null, null, '2017-12-16 06:40:03', '2017-12-16 06:40:03');
INSERT INTO `answers` VALUES ('418', '16', 'Wurtz reaction\r\n', null, null, '2017-12-16 06:40:03', '2017-12-16 06:40:03');
INSERT INTO `answers` VALUES ('419', '16', 'Wolfs reaction\r\n', null, '1', '2017-12-16 06:40:03', '2017-12-16 06:40:03');
INSERT INTO `answers` VALUES ('420', '16', 'Markomkov\r\n', null, null, '2017-12-16 06:40:03', '2017-12-16 06:40:03');
INSERT INTO `answers` VALUES ('421', '22', 'Tetra floro ethylene\r\n', null, null, '2017-12-16 06:40:45', '2017-12-16 06:40:45');
INSERT INTO `answers` VALUES ('422', '22', 'Starine\r\n', null, null, '2017-12-16 06:40:45', '2017-12-16 06:40:45');
INSERT INTO `answers` VALUES ('423', '22', 'ethyine\r\n', null, null, '2017-12-16 06:40:45', '2017-12-16 06:40:45');
INSERT INTO `answers` VALUES ('424', '22', 'propene nytraile\r\n', null, '1', '2017-12-16 06:40:45', '2017-12-16 06:40:45');
INSERT INTO `answers` VALUES ('425', '19', 'a) Bw_wjb\r\n', null, null, '2017-12-16 06:44:52', '2017-12-16 06:44:52');
INSERT INTO `answers` VALUES ('426', '19', '2-?????? ???????\r\n', null, null, '2017-12-16 06:44:52', '2017-12-16 06:44:52');
INSERT INTO `answers` VALUES ('427', '19', 'GwmwUwjb\r\n', null, null, '2017-12-16 06:44:52', '2017-12-16 06:44:52');
INSERT INTO `answers` VALUES ('428', '19', 'B&Dagger;_b\r\n', null, null, '2017-12-16 06:44:52', '2017-12-16 06:44:52');
INSERT INTO `answers` VALUES ('437', '54', 'hjkhkhjk\r\n', null, null, '2017-12-16 07:57:32', '2017-12-16 07:57:32');
INSERT INTO `answers` VALUES ('438', '54', 'hjkhjkhjkhjk\r\n', null, null, '2017-12-16 07:57:32', '2017-12-16 07:57:32');
INSERT INTO `answers` VALUES ('439', '54', 'hjkhjkhjkhjkhjk\r\n', null, '1', '2017-12-16 07:57:32', '2017-12-16 07:57:32');
INSERT INTO `answers` VALUES ('440', '54', 'jfgjhfgjhfjh nomanb\r\n', null, null, '2017-12-16 07:57:32', '2017-12-16 07:57:32');
INSERT INTO `answers` VALUES ('441', '3', ' Noman', 'Noman', '1', '2017-12-16 08:33:32', '2017-12-16 08:33:32');
INSERT INTO `answers` VALUES ('442', '55', 'sfsqdf\r\n', null, null, '2017-12-19 05:20:24', '2017-12-19 05:20:24');
INSERT INTO `answers` VALUES ('443', '55', 'qsfdsqdf\r\n', null, '1', '2017-12-19 05:20:24', '2017-12-19 05:20:24');
INSERT INTO `answers` VALUES ('444', '55', 'sfsdsd\r\n', null, null, '2017-12-19 05:20:24', '2017-12-19 05:20:24');
INSERT INTO `answers` VALUES ('445', '55', 'fsdgfqer\r\n', null, null, '2017-12-19 05:20:24', '2017-12-19 05:20:24');
INSERT INTO `answers` VALUES ('446', '56', 'sfsqdf\r\n', null, null, '2017-12-19 05:21:01', '2017-12-19 05:21:01');
INSERT INTO `answers` VALUES ('447', '56', 'qsfdsqdf\r\n', null, '1', '2017-12-19 05:21:01', '2017-12-19 05:21:01');
INSERT INTO `answers` VALUES ('448', '56', 'sfsdsd\r\n', null, null, '2017-12-19 05:21:01', '2017-12-19 05:21:01');
INSERT INTO `answers` VALUES ('449', '56', 'hh\r\n', null, null, '2017-12-19 05:21:01', '2017-12-19 05:21:01');
INSERT INTO `answers` VALUES ('450', '57', 'fgdfgdfgdfgdfg\r\n', null, null, '2017-12-19 05:39:11', '2017-12-19 05:39:11');
INSERT INTO `answers` VALUES ('451', '57', 'dfgdfgdf\r\n', null, '1', '2017-12-19 05:39:11', '2017-12-19 05:39:11');
INSERT INTO `answers` VALUES ('452', '57', 'dfgdfgdf\r\n', null, '1', '2017-12-19 05:39:11', '2017-12-19 05:39:11');
INSERT INTO `answers` VALUES ('453', '57', 'dfgfdgfdg\r\n', null, '1', '2017-12-19 05:39:11', '2017-12-19 05:39:11');
INSERT INTO `answers` VALUES ('454', '58', 'sdsa\r\n', null, '1', '2017-12-20 04:37:00', '2017-12-20 04:37:00');
INSERT INTO `answers` VALUES ('455', '58', 'sadsad\r\n', null, null, '2017-12-20 04:37:00', '2017-12-20 04:37:00');
INSERT INTO `answers` VALUES ('456', '58', 'sd\r\n', null, null, '2017-12-20 04:37:00', '2017-12-20 04:37:00');
INSERT INTO `answers` VALUES ('457', '58', 'ds\r\n', null, null, '2017-12-20 04:37:00', '2017-12-20 04:37:00');
INSERT INTO `answers` VALUES ('462', '59', 'sad sa sads sadd&nbsp;\r\n', null, null, '2017-12-20 04:44:53', '2017-12-20 04:44:53');
INSERT INTO `answers` VALUES ('463', '59', 'sad sad sad sad sa\r\n', null, null, '2017-12-20 04:44:53', '2017-12-20 04:44:53');
INSERT INTO `answers` VALUES ('464', '59', 'sad asdsa\r\n', null, '1', '2017-12-20 04:44:53', '2017-12-20 04:44:53');
INSERT INTO `answers` VALUES ('465', '59', '&nbsp;dsadsad&nbsp;\r\n', null, null, '2017-12-20 04:44:53', '2017-12-20 04:44:53');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'Science', '1', '2016-11-29 06:50:51', '2016-11-29 06:50:51');
INSERT INTO `groups` VALUES ('2', 'Business', '1', '2016-11-29 06:51:01', '2016-11-29 06:51:01');
INSERT INTO `groups` VALUES ('3', 'Arts', '1', '2016-11-29 06:51:20', '2016-11-29 06:51:20');

-- ----------------------------
-- Table structure for `questions`
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_id` int(10) unsigned DEFAULT '1',
  `time` int(10) unsigned DEFAULT '0',
  `question` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `point` float DEFAULT '1',
  `solution` text,
  `answer_description` longtext,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of questions
-- ----------------------------
INSERT INTO `questions` VALUES ('3', '6', '0', '<p>IUPAC name : (CH<sub><small>3</small></sub>)<sub>3</sub>C - CH<sub>2</sub>CH(CH<sub>3</sub>)<sub>2</sub></p>\r\n', '1', null, '', '2016-11-30 21:34:19', '2017-02-10 03:17:07');
INSERT INTO `questions` VALUES ('4', '6', '0', '<p>নিচের কোনটি জ্যামিতিক সমানুতা প্রদর্শন করে ?</p>\r\n', '1', null, '', '2016-11-30 22:13:23', '2017-03-15 06:24:46');
INSERT INTO `questions` VALUES ('5', '6', '0', '<p>কোনটি হেক্সেনের সমানু নয় ?</p>\r\n', '1', null, '', '2016-11-30 22:20:16', '2017-03-15 06:25:46');
INSERT INTO `questions` VALUES ('6', '6', '0', '<p>CaC<sub>2</sub>&nbsp;এর উপর দিয়ে পানি চালনা করলে কি হয় ?</p>\r\n', '1', null, '', '2016-11-30 22:24:29', '2017-03-15 06:26:01');
INSERT INTO `questions` VALUES ('8', '6', '0', '<p>আলোক সক্রিয়াতা পরিমাপক যন্ত্রের নাম কি ?</p>\r\n', '1', null, '', '2016-11-30 22:34:00', '2017-03-15 06:26:50');
INSERT INTO `questions` VALUES ('9', '6', '0', '<p>কোন অ্যালকাইল ফ্রি - রেডিক্যালটির স্থায়িত্ব সর্বাধিক ?</p>\r\n', '1', null, '', '2016-11-30 22:48:19', '2017-03-15 06:27:06');
INSERT INTO `questions` VALUES ('10', '6', '0', '<p>D &ndash;Lactic acid এর আপেক্ষিক আবর্তন কত ?</p>\r\n', '1', null, '', '2016-11-30 22:51:44', '2017-03-15 06:27:23');
INSERT INTO `questions` VALUES ('11', '6', '0', 'কোনটি কেন্দ্রাকর্ষী বিকারক নয় ?', '1', null, '', '2016-11-30 22:56:21', '2016-11-30 22:56:21');
INSERT INTO `questions` VALUES ('12', '6', '0', 'HC=C-CH=CH যোগটিতে C-C একক বন্ধনটিতে কার্বনের যে সংকরন ঘটে তাহলো-\r\n', '1', null, '', '2016-11-30 23:02:47', '2016-11-30 23:02:47');
INSERT INTO `questions` VALUES ('13', '6', '0', 'CH=CH জারনের ফলে কি হয় ? ', '1', null, '', '2016-11-30 23:06:34', '2016-11-30 23:06:34');
INSERT INTO `questions` VALUES ('14', '6', '0', 'ইথানল বাষ্প উচ্চতাপমাত্রায় Al<sub>2</sub>O<sub>3</sub> এর উপর দিয়ে চালনা যে দ্রব্য পাওয়া যায় তা হচ্ছে-\r\n', '1', null, '', '2016-11-30 23:18:16', '2017-12-16 06:24:38');
INSERT INTO `questions` VALUES ('15', '6', '0', '<p>Anhydrous AlCl<sub>3</sub> এর উপস্থিতিতে এসিটাইল ক্লোরাইড এর সাথে বেনজিন সামান্য উত্তপ্ত করলে যে দ্রব্য উৎপন্ন হয় তা হচ্ছে -</p>\r\n', '1', null, '', '2016-11-30 23:34:18', '2016-12-05 09:54:23');
INSERT INTO `questions` VALUES ('17', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">&Dagger;ebwRb bvB&Dagger;U&ordf;kb wew&micro;qvq &dagger;Kvb wew&micro;qK &dagger;mUwU e&uml;e&uuml;Z nq ?</span></span></span></p>\r\n', '1', null, '', '2016-12-05 00:48:17', '2016-12-05 00:50:38');
INSERT INTO `questions` VALUES ('18', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">wb&Dagger;Pi &dagger;KvbwU IRbxKib I Av`&ordf;&copy;we&Dagger;k&shy;l&Dagger;Yi d&Dagger;j &iuml;ay GKwU wew&micro;qK w`&Dagger;e ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 00:52:44', '2016-12-05 00:54:34');
INSERT INTO `questions` VALUES ('19', '6', '0', 'মার্কনিকভ নিয়ম অনুযায়ী wb&Dagger;Pi বিক্রিয়ায় &dagger;Kvb &dagger;h&Scaron;MwU Drcb&oelig; nq?\r\n', '1', null, 'dfuhgufdhgufdhguhdgud\r\n', '2016-12-05 01:00:32', '2017-12-16 06:44:52');
INSERT INTO `questions` VALUES ('20', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">wb&Dagger;Pi &dagger;Kvb g~jKwU &dagger;ebwRb P&Dagger;&micro; c&Ouml;wZ&macr;&rsquo;vcb wew&micro;qv &dagger;gUv wb&Dagger;`&copy;kK ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 01:02:27', '2016-12-05 01:04:47');
INSERT INTO `questions` VALUES ('21', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">dzU&scaron;&mdash; UjyB&Dagger;bi g&Dagger;a&uml; &dagger;K&not;vwib M&uml;vm Pvjbv Ki&Dagger;j wK Drcb&oelig; nq &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-05 01:07:01', '2016-12-05 01:07:01');
INSERT INTO `questions` VALUES ('22', '6', '0', 'dsadsa sadsa\r\n', '1', null, 'dsadsad\r\n', '2016-12-05 01:09:30', '2017-12-16 06:40:45');
INSERT INTO `questions` VALUES ('23', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">c&Dagger;&AElig;vwjqvg &dagger;Zj GKwU &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-05 01:13:05', '2016-12-05 10:17:39');
INSERT INTO `questions` VALUES ('25', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">wb&Dagger;Pi &Dagger;KvbwU &dagger;Udj&Dagger;bi ms&Dagger;KZ ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:30:31', '2016-12-05 09:31:35');
INSERT INTO `questions` VALUES ('26', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">wK&Dagger;mi Dcw&macr;&rsquo;wZ&Dagger;Z A&uml;vj&Dagger;K&Dagger;bi mgvbyKiY nq ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:33:06', '2016-12-05 09:41:13');
INSERT INTO `questions` VALUES ('27', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">A&uml;vjwK&Dagger;bi w&Oslash;&Uuml;&Dagger;bi Ae&macr;&rsquo;vb wbY&copy;&Dagger;q &dagger;Kvb wew&micro;qvwU c&Ouml;&Dagger;hvR&uml; ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:35:34', '2016-12-05 09:35:34');
INSERT INTO `questions` VALUES ('28', '6', '0', '<p><span style=\"font-size:9.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">ফেনলের সাথে</span></span></span> <span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">Zn </span></span></span><span style=\"font-size:9.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">এর বিক্রিয়ায় কি উৎপন্ন হয়</span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\"> ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:37:18', '2016-12-05 09:38:25');
INSERT INTO `questions` VALUES ('29', '6', '0', '<p><span style=\"color:black; font-family:Vrinda; font-size:9.0pt\">অ্যালডিহাইডের কার্যকরী মূলকের গাঠনিক সংকেত</span><span style=\"color:black; font-family:SutonnyMJ; font-size:12.0pt\"> &Ntilde;</span></p>\r\n', '1', null, '', '2016-12-05 09:42:53', '2016-12-05 09:43:15');
INSERT INTO `questions` VALUES ('30', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">&Dagger;Kvb</span></span></span><span style=\"font-size:9.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">টি অ্যলিসাইক্লিক যৌগ</span></span></span><span style=\"font-size:9.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\"> ? </span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:44:25', '2016-12-05 09:44:25');
INSERT INTO `questions` VALUES ('31', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">&Dagger;K&not;v&Dagger;iv&Dagger;ebwRb I A&uml;v&Dagger;gvwbqv </span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Palatino Linotype&quot;,serif\"><span style=\"color:black\">200<sup>0</sup>C </span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">Zvcgv&Icirc;vq<strong> </strong>I D&rdquo;P Pvc c&Ouml;fve&Dagger;Ki Dcw&macr;&rsquo;wZ&Dagger;Z Drcb&oelig; K&Dagger;i &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:45:41', '2016-12-05 09:45:41');
INSERT INTO `questions` VALUES ('32', '6', '0', '<p><span style=\"color:black; font-family:Vrinda; font-size:9.0pt\">অ্যসিটিলিনের পলিমার---</span></p>\r\n', '1', null, '', '2016-12-05 09:46:51', '2016-12-05 10:16:13');
INSERT INTO `questions` VALUES ('33', '6', '0', '<p><span style=\"font-size:10.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">মার্শ গ্যাসের সংকেত&mdash;</span></span></span></p>\r\n', '1', null, '', '2016-12-05 09:48:12', '2016-12-05 09:51:23');
INSERT INTO `questions` VALUES ('34', '6', '0', '<p><span style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">অ্যলকেন নাইট্রালকে অম্লীয় মাধ্যমে অ।দ্র বিশ্লেষন </span></span></span><span style=\"font-size:12.5pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">__</span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">করলে হয়</span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:25:59', '2016-12-11 23:25:59');
INSERT INTO `questions` VALUES ('35', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">রেকটিফাইড স্প্রিট</span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">__</span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:27:51', '2016-12-11 23:30:30');
INSERT INTO `questions` VALUES ('36', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">M&Ouml;xMbvW&copy; weKvi&Dagger;Ki mv&Dagger;_ wg_vb&Dagger;ji wew&micro;qvq wK Drcb&oelig; nq ? </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:31:55', '2016-12-11 23:31:55');
INSERT INTO `questions` VALUES ('37', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">&Dagger;K&not;v&Dagger;iv&Dagger;ebwRb I A&uml;v&Dagger;gvwbqv </span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Palatino Linotype&quot;,serif\"><span style=\"color:black\">200<sup>0</sup>C </span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">Zvcgv&Icirc;vq<strong> </strong>I D&rdquo;P Pvc c&Ouml;fve&Dagger;Ki Dcw&macr;&rsquo;wZ&Dagger;Z Drcb&oelig; K&Dagger;i &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:33:17', '2016-12-11 23:33:17');
INSERT INTO `questions` VALUES ('38', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">wg&Dagger;_b I &dagger;K&not;vwib M&uml;v&Dagger;mi wgk&ordf;b mivmwi m~&Dagger;h&copy;i Av&Dagger;jv&Dagger;Z wb&Dagger;j &dagger;h we&Dagger;&ugrave;vib N&Dagger;U Zv&Dagger;Z Drcv` n&Dagger;jv &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:35:42', '2016-12-11 23:36:35');
INSERT INTO `questions` VALUES ('39', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Palatino Linotype&quot;,serif\"><span style=\"color:black\">CHCl<sub>3</sub> </span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">Gi Aw&macr;&mdash;Z&iexcl; c&Ouml;gvb Kiv hvq &dagger;Kvb cix&para;vi mvnv&Dagger;h&uml; ? </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:37:49', '2016-12-11 23:37:49');
INSERT INTO `questions` VALUES ('40', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">M&Ouml;xMbvW&copy; weKvi&Dagger;Ki mv&Dagger;_ GKwU wK&Dagger;Uvb wew&micro;qv Ki&Dagger;j wK Drcb&oelig; nq &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:39:24', '2016-12-11 23:40:03');
INSERT INTO `questions` VALUES ('41', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">m&curren;c&Ouml;wZ e&uml;vcKfv&Dagger;e e&uml;e&uuml;Z n&uml;v&Dagger;jv&Dagger;R&Dagger;bv A&uml;vj&Dagger;Kb d&not;z&Dagger;_b GKwU &Ntilde; </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:48:51', '2016-12-11 23:48:51');
INSERT INTO `questions` VALUES ('42', '6', '0', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">wb&Dagger;Pi &dagger;KvbwU Zvr&para;wbKfv&Dagger;e jyKvm wew&micro;qv &dagger;`q ? </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:51:07', '2016-12-11 23:52:26');
INSERT INTO `questions` VALUES ('43', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">jyKvm weKviK &dagger;KvbwUi mv&Dagger;_ wew&micro;qv K&Dagger;i bv ? </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:53:59', '2016-12-11 23:55:30');
INSERT INTO `questions` VALUES ('44', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Palatino Linotype&quot;,serif\"><span style=\"color:black\">PCl<sub>5 </sub>&nbsp;&amp; C<sub>2</sub>H<sub>5</sub>OH </span></span></span><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">Gi wew&micro;qvq &dagger;KvbwU Drcb&oelig; nq ? </span></span></span></p>\r\n', '1', null, '', '2016-12-11 23:56:50', '2016-12-11 23:56:50');
INSERT INTO `questions` VALUES ('45', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:SutonnyMJ\"><span style=\"color:black\">&Dagger;dbj mbv&sup3;Ki&Dagger;b wjevig&uml;vb cix&para;vq wb&Dagger;Pi &dagger;KvbwU c&Ouml;&Dagger;qvRb -</span></span></span></p>\r\n', '1', null, '', '2016-12-12 00:06:39', '2016-12-12 00:09:06');
INSERT INTO `questions` VALUES ('46', '6', '0', 'sadsadsa\r\n', '1', null, '', '2016-12-12 00:10:13', '2017-12-16 06:23:39');
INSERT INTO `questions` VALUES ('47', '6', '0', '<p><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">ফেনলের নাইট্রেশনে কি হয়?</span></p>\r\n', '1', null, '', '2016-12-12 00:11:08', '2016-12-12 00:11:08');
INSERT INTO `questions` VALUES ('48', '6', '0', '<p><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">অ্যলকোহলের অম্লধর্মীতার ক্রম</span><span style=\"color:black; font-family:&quot;Times New Roman&quot;,serif; font-size:10.0pt\">__</span></p>\r\n', '1', null, '', '2016-12-12 00:13:03', '2016-12-12 00:14:41');
INSERT INTO `questions` VALUES ('49', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">What is Prestone ?</span></span></span></p>\r\n', '1', null, '', '2016-12-12 00:16:08', '2016-12-12 00:17:34');
INSERT INTO `questions` VALUES ('50', '6', '0', '<p><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">ফেনলের চেয়ে পিকরিক এসিড</span><span style=\"color:black; font-family:&quot;Times New Roman&quot;,serif; font-size:10.0pt\">___</span><span style=\"color:black; font-family:Vrinda; font-size:10.0pt\">গুন বেশি অম্লীয়</span></p>\r\n', '1', null, '', '2016-12-12 00:18:22', '2016-12-12 00:18:22');
INSERT INTO `questions` VALUES ('51', '6', '0', '<p style=\"text-align:justify\"><span style=\"font-size:10.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">ফেনলের সাথে </span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">CHCl<sub>3</sub></span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">ও ক্ষারের জলীয় দ্রবনকে উত্তপ্ত করলে</span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><span style=\"color:black\">____</span></span></span><span style=\"font-size:10.0pt\"><span style=\"font-family:Vrinda\"><span style=\"color:black\">হয়</span></span></span></p>\r\n', '1', null, '', '2016-12-12 00:20:47', '2016-12-12 00:41:36');
INSERT INTO `questions` VALUES ('52', '1', '0', 'gfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gf\r\n', '1', null, 'sadsadsa&nbsp;gfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gfgfddgfd df gfd gdffd gf\r\n', '2017-12-16 05:57:41', '2017-12-16 05:57:41');
INSERT INTO `questions` VALUES ('54', '4', '0', 'mhgkjkjhkhjkhjkjhkjk\r\n', '1', null, 'hkhjkhjkhjkhjkhjkhjkhjkhjkhkhkh nomam\r\n', '2017-12-16 07:55:32', '2017-12-16 07:57:32');
INSERT INTO `questions` VALUES ('55', '1', '0', 'sfjhfqsd\r\n', '1', null, 'serfsqdf\r\n', '2017-12-19 05:20:24', '2017-12-19 05:20:24');
INSERT INTO `questions` VALUES ('56', '1', '0', 'sfjhfqsd\r\n', '1', null, 'serfsqdf\r\n', '2017-12-19 05:21:01', '2017-12-19 05:21:01');
INSERT INTO `questions` VALUES ('57', '1', '0', 'fdgfdgdfgfdgdfgdfgfdgfjhgjbnmvbcvbxvxvz\r\n', '1', null, 'fgdfgdfgfdg\r\n', '2017-12-19 05:39:11', '2017-12-19 05:39:11');
INSERT INTO `questions` VALUES ('58', '1', '0', 'sdad\r\n', '1', null, 'das sadsa sad asd\r\n', '2017-12-20 04:37:00', '2017-12-20 04:37:00');
INSERT INTO `questions` VALUES ('59', '1', '0', 'Who is&nbsp;\r\n', '1', null, 'asd&nbsp;\r\n', '2017-12-20 04:43:36', '2017-12-20 04:44:53');

-- ----------------------------
-- Table structure for `results`
-- ----------------------------
DROP TABLE IF EXISTS `results`;
CREATE TABLE `results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mark` float(11,0) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of results
-- ----------------------------
INSERT INTO `results` VALUES ('1', '1', '1', '34', '2017-03-17 12:59:19', '2017-03-17 12:59:19');
INSERT INTO `results` VALUES ('2', '1', '2', '35', null, null);
INSERT INTO `results` VALUES ('3', '1', '1', '34346500096', '2017-12-20 05:43:32', '2017-12-20 05:43:32');

-- ----------------------------
-- Table structure for `subjects`
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of subjects
-- ----------------------------

-- ----------------------------
-- Table structure for `tests`
-- ----------------------------
DROP TABLE IF EXISTS `tests`;
CREATE TABLE `tests` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject_id` int(10) unsigned DEFAULT '1',
  `group_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `instructions` text,
  `test_time` int(10) unsigned DEFAULT '0',
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tests
-- ----------------------------
INSERT INTO `tests` VALUES ('1', '1', '1', 'Test', null, '100', '1', null, null, null);

-- ----------------------------
-- Table structure for `tests_questions`
-- ----------------------------
DROP TABLE IF EXISTS `tests_questions`;
CREATE TABLE `tests_questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `test_id` int(10) DEFAULT NULL,
  `question_id` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tests_questions
-- ----------------------------
INSERT INTO `tests_questions` VALUES ('20', '1', '11', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('21', '1', '12', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('22', '1', '13', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('23', '1', '14', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('24', '1', '15', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('26', '1', '17', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('27', '1', '18', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('28', '1', '19', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('29', '1', '20', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('30', '1', '21', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('31', null, '22', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('32', null, '23', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('33', null, '25', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('34', null, '26', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('35', null, '27', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('36', null, '28', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('37', null, '29', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('38', null, '30', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('39', null, '31', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('40', null, '32', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('41', null, '33', '2016-12-06 01:01:46', '2016-12-06 01:01:46');
INSERT INTO `tests_questions` VALUES ('42', '1', '3', null, null);
INSERT INTO `tests_questions` VALUES ('43', '1', '4', null, null);
INSERT INTO `tests_questions` VALUES ('44', '1', '5', null, null);
INSERT INTO `tests_questions` VALUES ('45', '1', '6', null, null);
INSERT INTO `tests_questions` VALUES ('47', '1', '8', null, null);
INSERT INTO `tests_questions` VALUES ('48', '1', '9', null, null);
INSERT INTO `tests_questions` VALUES ('49', '1', '10', null, null);
INSERT INTO `tests_questions` VALUES ('50', '1', '11', null, null);
INSERT INTO `tests_questions` VALUES ('51', '1', '12', null, null);
INSERT INTO `tests_questions` VALUES ('52', '1', '13', null, null);
INSERT INTO `tests_questions` VALUES ('53', '1', '14', null, null);
INSERT INTO `tests_questions` VALUES ('54', '1', '15', null, null);
INSERT INTO `tests_questions` VALUES ('56', '1', '17', null, null);
INSERT INTO `tests_questions` VALUES ('57', '1', '18', null, null);
INSERT INTO `tests_questions` VALUES ('58', '1', '19', null, null);
INSERT INTO `tests_questions` VALUES ('59', '1', '20', null, null);
INSERT INTO `tests_questions` VALUES ('60', '1', '21', null, null);
INSERT INTO `tests_questions` VALUES ('61', '1', '22', null, null);
INSERT INTO `tests_questions` VALUES ('62', '1', '23', null, null);
INSERT INTO `tests_questions` VALUES ('63', '1', '25', null, null);
INSERT INTO `tests_questions` VALUES ('64', '1', '26', null, null);
INSERT INTO `tests_questions` VALUES ('65', '1', '27', null, null);
INSERT INTO `tests_questions` VALUES ('66', '1', '28', null, null);
INSERT INTO `tests_questions` VALUES ('67', '1', '29', null, null);
INSERT INTO `tests_questions` VALUES ('68', '1', '30', null, null);
INSERT INTO `tests_questions` VALUES ('69', '1', '31', null, null);
INSERT INTO `tests_questions` VALUES ('70', '1', '32', null, null);
INSERT INTO `tests_questions` VALUES ('71', '1', '33', null, null);
INSERT INTO `tests_questions` VALUES ('72', '1', '34', null, null);
INSERT INTO `tests_questions` VALUES ('73', '1', '35', null, null);
INSERT INTO `tests_questions` VALUES ('74', '1', '36', null, null);
INSERT INTO `tests_questions` VALUES ('75', '1', '37', null, null);
INSERT INTO `tests_questions` VALUES ('76', '1', '38', null, null);
INSERT INTO `tests_questions` VALUES ('77', '1', '39', null, null);
INSERT INTO `tests_questions` VALUES ('78', '1', '40', null, null);
INSERT INTO `tests_questions` VALUES ('79', '1', '41', null, null);
INSERT INTO `tests_questions` VALUES ('80', '1', '42', null, null);
INSERT INTO `tests_questions` VALUES ('81', '1', '43', null, null);
INSERT INTO `tests_questions` VALUES ('82', '1', '44', null, null);
INSERT INTO `tests_questions` VALUES ('83', '1', '45', null, null);
INSERT INTO `tests_questions` VALUES ('84', '1', '46', null, null);
INSERT INTO `tests_questions` VALUES ('85', '1', '47', null, null);
INSERT INTO `tests_questions` VALUES ('86', '1', '48', null, null);
INSERT INTO `tests_questions` VALUES ('87', '1', '49', null, null);
INSERT INTO `tests_questions` VALUES ('88', '1', '50', null, null);
INSERT INTO `tests_questions` VALUES ('89', '1', '51', null, null);
INSERT INTO `tests_questions` VALUES ('90', '1', '52', null, null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '0', 'test@ds.sds', '324324', 'test', '1990711536');

-- ----------------------------
-- Table structure for `users_answers`
-- ----------------------------
DROP TABLE IF EXISTS `users_answers`;
CREATE TABLE `users_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `test_type` varchar(20) DEFAULT NULL,
  `ans_index` varchar(10) DEFAULT NULL,
  `is_flag` tinyint(1) DEFAULT '0',
  `ques_offset` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users_answers
-- ----------------------------
INSERT INTO `users_answers` VALUES ('1', '3', null, null, '441', null, null, '0', null, null, null);
INSERT INTO `users_answers` VALUES ('2', '1', '1', '11', '57', 'mock_test', 'A', '0', null, '2018-01-22 18:38:42', '2018-01-22 18:48:57');
INSERT INTO `users_answers` VALUES ('3', '1', '1', '12', '61', 'mock_test', 'A', '0', null, '2018-01-22 18:49:02', '2018-01-22 18:49:02');
INSERT INTO `users_answers` VALUES ('4', '1', '1', '13', '67', 'mock_test', 'C', '0', null, '2018-01-22 18:49:07', '2018-01-22 18:49:07');
INSERT INTO `users_answers` VALUES ('5', '1', '1', '17', '112', 'mock_test', 'D', '0', null, '2018-01-22 18:49:26', '2018-01-22 18:49:26');
INSERT INTO `users_answers` VALUES ('6', '1', '1', '15', null, 'mock_test', null, '1', null, '2018-01-22 18:50:04', '2018-01-22 18:50:04');
