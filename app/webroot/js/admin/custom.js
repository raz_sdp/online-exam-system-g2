/**
 * Created by admin on 12/15/17.
 */
$(function(){
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    })
    var user_id = $('#user_id').val()?$('#user_id').val():1;

    /*add user payment section*/
    $('.js-user-category-ck-box').on('change', function () {
        var payment_val = $('#PaymentAmount' + $(this).val()).val();
        if ($(this).is(':checked')) {
            $(this).parent().parent().find('.payment-input-field').removeAttr('disabled');
            /*Price Calculation*/
            paymentCalculation();
        } else {
            $(this).parent().parent().find('.payment-input-field').attr('disabled', true);
            paymentCalculation();
        }
    });

    /*Price Select*/
    $('.payment-select-price').on('change', function () {
        var payment_val = $('.payment-select-price').val();
        paymentCalculation();
    });

    $("select[id^='PaymentTestidsArray']").on('change', function () {
        paymentCalculation();
    });
    /*Date pick*/
/*
    $('.date-pick').datetimepicker({
        viewMode: 'years'
    });
*/

    /*no of question change*/
    $('#QuestionNoOfAns').on('change', function(){
        var no_of_ans = $(this).val();
        var question_id = $('#test_id').val();
        var data = {
            no_of_ans : no_of_ans,
            question_id : question_id
        };
        $.post(ROOT+'/questions/get_choice_lists',data, function(data){
            $('.js-ans-input-box').html(data);
        });
    });

    /*Assign question into Test*/
    $(".js-dynamic-question-box").on('change','#TestsQuestionQuestionId', function () {
        questionHandle();
    });

    /*Onchange Subject of assignment Question into Test*/
    $('#TestsQuestionTestId, #TestsQuestionSubjectId').on('change', function(){
        var subject_id = $('#TestsQuestionSubjectId').val();
        var test_id = $('#TestsQuestionTestId').val();
        $.get(ROOT+'tests_questions/create_question_select_box/'+subject_id+'/'+test_id, function(data){
            $('.js-dynamic-question-box').html(data);
            questionHandle();
        });
    });

    /*Mock Test Submit Answer*/
    $('.js-exam-held').on('change','.js-answer', function(){
        if($(this).is(':checked')){
            is_multiple_ans = false;
            $('#submit-answer-btn,#q-bank-submit-answer-btn').removeAttr('disabled');
        } else {
            $('#submit-answer-btn,#q-bank-submit-answer-btn').attr('disabled', true);
        }
    });
    /**
     * check multiple select box if selected or not
     * @returns {boolean}
     */
    $.fn.allSelected = function(){
        return this.filter(function(){
            return parseInt(this.value, 10) > 0;
        }).length >= this.length;
    };
    $('.js-exam-held').on('change','.js-multi-ques-ans', function(){
        if($('.js-multi-ques-ans').allSelected()){
            is_multiple_ans = true;
            $('#submit-answer-btn,#q-bank-submit-answer-btn').removeAttr('disabled');
        } else {
            is_multiple_ans = false;
            $('#submit-answer-btn,#q-bank-submit-answer-btn').attr('disabled', true);
        }
    });
    /*Checked Answer click everywhere but doesn't work properly*/
    /*$('.js-exam-held').on('click','#answer-box li', function(){
     $(this).find('.js-answer').attr('checked', true);
     if($(this).find('.js-answer').is(':checked')) {
     $('#submit-answer-btn').removeAttr('disabled');
     } else{
     $('#submit-answer-btn').attr('disabled', true);
     }
     });*/

    /*For Mock Test Answer Submit*/
    $('.js-exam-held').on('click','#submit-answer-btn', function () {
        var total_question = $('#total_question').val();
        // offset = offset >= total_question-1 ? 0 : offset+1;
        /*if(parseInt($('#first_offset').val())) {
         offset = parseInt($('#first_offset').val());
         } else {
         offset = parseInt($('#offset').val());
         }*/
        offset = parseInt($('#offset').val());
        offset = offset >= total_question-1 ? 0 : offset+1;
        console.log(offset)
        submitAnswer(offset);
    });
    /*</For Mock Test Answer Submit>*/
    /*<For Question Bank Answer Submit>*/
    $('.js-exam-held').on('click','#q-bank-submit-answer-btn', function () {
        var total_question = $('#total_question').val();
        //offset = offset >= total_question-1 ? 0 : offset;
        if(parseInt($('#first_offset').val())) {
            offset = parseInt($('#first_offset').val());
        } else {
            offset = parseInt($('#offset').val());
        }
        questionBankPracticeSubmitAnswer(offset);
    });
    $('.js-exam-held').on('click', '#js-q-bank-prev-btn', function () {
        var total_question = $('#total_question').val();
        var review = $('#review').val();
        if(review){
            offset = parseInt($('#offset').val());
        }
        if(parseInt($('#first_offset').val())) {
            offset = parseInt($('#first_offset').val());
            offset--;
            if(offset==-1){
                offset = total_question -1;
            }
        } else {
            offset = parseInt($('#offset').val());
            offset--;
            if(offset == 1 || offset == -1){
                offset = total_question -1;
            }
        }
        oneByOneQuestionBankHandle(offset);
    });
    $('.js-exam-held').on('click','.js-q-bank-next-btn', function(){
        console.log(offset)
        var total_question = $('#total_question').val();
        var review = parseInt($('#review').val());
        if(review){
            offset = parseInt($('#offset').val());
        }
        offset = parseInt($('#offset').val());
        offset = offset >= total_question-1 ? 0 : offset+1;
        oneByOneQuestionBankHandle(offset);
    });
    /*</For Question Bank Answer Submit>*/

    /*<For mock test>*/
    $('.js-exam-held').on('click', '#js-prev-btn', function (e) {
        var total_question = $('#total_question').val();
        //console.log(parseInt($('#first_offset').val()))
        if (!isNaN(parseInt($('#first_offset').val()))) {
            offset = parseInt($('#first_offset').val());
            offset--;
            //console.log(offset)
            if (offset == -1) {
                //offset = total_question -1;
                // return false;
                $(this).prop('disabled', true);
            } else {
                $('#js-next-btn').removeAttr('disabled');
                oneByOneQuestionHandle(offset);
            }
        } else {
            console.log(offset)
            offset = parseInt($('#offset').val());
            offset--;
            if (offset == -1) {
                //offset = total_question -1;
                $(this).prop('disabled', true);
            } else  {
                $('#js-next-btn').removeAttr('disabled');
                oneByOneQuestionHandle(offset);
            }
        }

        /*offset--;
         if(offset==-1){
         offset = total_question -1;
         }*/
    });
    $('.js-exam-held').on('click','#js-next-btn', function(){
        var total_question = $('#total_question').val();
        offset = parseInt($('#offset').val());
        if (offset >= total_question-1) {
            $(this).prop('disabled', true);
        } else {
            $('#js-prev-btn').removeAttr('disabled');
            offset = offset+1;
            oneByOneQuestionHandle(offset);
        }
        //offset = offset >= total_question-1 ? 0 : offset+1;
    });
    /*</For mock test>*/
    /*Flag funct.*/
    $('.js-exam-held').on('click','.js-flag', function(){
        $(this).toggleClass('red-flag');
        var test_id = $('#test_id').val();
        var test_type = $('#action').val() == 'held_exam' ? 'mock_test' : 'question_bank';
        var question_id = $(this).closest('button').val();
        var is_flag = 0;
        if($(this).hasClass('red-flag')){
            is_flag = 1;
        } else{
            is_flag = 0;
        }
        $.get(ROOT + 'users_answers/users_answer/'+user_id+'/'+is_flag+'/'+test_id+'/'+question_id+'/'+test_type, function(response){
            if(response.status){
                if(is_flag){
                    $('#jump-question-list li[rel='+question_id+']>a').append('<span class="glyphicon glyphicon-flag red-flag" style="margin-left: 40%"></span>');
                    $('#js-flag-'+question_id).html('<span class="glyphicon glyphicon-flag red-flag"></span>');
                } else{
                    $('#jump-question-list li[rel='+question_id+']>a').find('.red-flag').remove();
                    $('#js-flag-'+question_id).find('span').remove();
                }
            }
        },'json');
    });
    $('.js-exam-held').on('click','.js-question-li', function(){
        var offset_li = parseInt($(this).attr('id'))-1;
        offset = offset_li;
        oneByOneQuestionHandle(offset);
    });
    /*Question bank */
    $('.js-exam-held').on('click','.js-q-bank-question-li', function(){
        var li_value = $(this).attr('id').split('-')[0].trim();
        var offset_li = parseInt(li_value)-1;
        //offset = offset_li;
        oneByOneQuestionBankHandle(offset_li);
    });
    /*Add Note funct*/
    $('.js-exam-held').on('click','#savenotes', function(){
        var _this = $(this);
        _this.text('Saving...');
        _this.attr('disabled', true);
        var question_id = $("#question-id-button").val();
        var test_id = $('#test_id').val();
        var data = {
            "data[Note][user_id]" :user_id,
            "data[Note][question_id]" :question_id,
            "data[Note][test_id]" : test_id,
            "data[Note][text]" : $('.summernote').code()
        };
        $.post(ROOT+'notes/add', data,  function(response){
            _this.removeAttr('disabled');
            _this.text('Save my notes');
        }, 'json');
    });
    /* Summer note editor */
    /*Text Editing plugin*/
    //$('.summernote').summernote();
    /*Save and exit*/
    $('.js-save-exit').on('click', function(){
        swal({
            title: "Mock Test Info",
            text: "Are you sure you want to leave the exam and finish it later?",
            type: "info",
            showCancelButton: true, closeOnConfirm: true, showLoaderOnConfirm: true
        }, function () {
            var test_id = $('#test_id').val();
            var exit_time = $('#timer').text();
            var data = {
                'data[Saveandexit][user_id]': user_id,
                'data[Saveandexit][test_id]': test_id,
                'data[Saveandexit][exit_time]': exit_time
            };
            $.post(ROOT + 'saveandexits/save_and_exit',data,  function(response){
                console.log(response)
                if(response.success) {
                    window.location = ROOT+'tests/mock_test/'+response.redirect_cat_id;
                }
            },'json');
        });
    });
    $('.js-finish-result').on('click', function(){
        var test_id = $('#test_id').val();
        var total_question = $('#total_question').val();
        var data = {
            'data[Saveandexit][user_id]': user_id,
            'data[Saveandexit][test_id]': test_id,
            'data[Saveandexit][exit_time]': 1
        };
        $.post(ROOT + 'saveandexits/save_and_exit',data,  function(response){
            //console.log(response)
            if(response.success) {
                $.get(ROOT + 'tests_questions/ajax_held_exam/'+test_id+'/1/'+offset+'/'+total_question+'/'+true, function(response){
                    $('.exam-box').html(response);
                });
            }  else{
                sweetAlert("Oops...", "Something went wrong! Please try again.", "error");
            }
            $('#exam-menu').hide();
            $('#result-menu').show();
        },'json');
    });
    /*End Review in Question Bank*/
    $('.js-end-review').on('click', function(){
        $('#exam-menu-2').hide();
        $('#result-menu-2').show();
        var test_id = $('#test_id').val();
        var total_question = $('#total_question').val();
        //var review = $('#review').val();
        var review = 1;
        var test_type = $('#js-test-type').val();
        $.get(ROOT + 'tests_questions/ajax_question_bank_practice/'+test_id+'/1/'+offset+'/'+total_question+'/'+review+'/'+test_type, function(response){
            $('.js-exam-held').html(response);
            $('html,body').animate({ scrollTop: 0 },400);
        });
    });


    /*Password Generate*/
    $('#pass-generate').on('click', function(){
        $('#password').val(randomPassword(8));
    });

    /*Display Test*/
    $('.js-tests').on('click', function(){
        var test_ids = $(this).prev('.js_test_ids').val();
        //console.log(test_ids);
        $.post(ROOT+'admin/tests/display_test_wrt_id', {'test_ids' : test_ids}, function(res){
            $('#js-test-box').html(res);
        });
    });

    /*Handle Multiple Question from Admin*/
    $('.js-show-mul-question').on('change', function () {
        console.log($(this)[0].checked)
        if($(this)[0].checked){
            $('.js-related-question-list').show();
            $('.common-disable-elem').find("*").removeProp("disabled");
        } else {
            $('.js-related-question-list').hide();
            $('.common-disable-elem').prop('disabled', true);
        }
    });


    /*For Add new Item*/
    $('.js-show-mul-question-add').on('change', function () {
        var subject_id = $('#QuestionSubjectId').val();
        console.log($(this)[0].checked)
        if($('.js-show-mul-question-add')[0].checked){
            $.get(ROOT+'questions/create_selectbox_for_related_question/'+subject_id, function (response) {
                $('.js-related-question-list').append(response);
                $('.js-related-question-list').show();
                $('.js-related-question-list').removeAttr('disabled');
            });
        }
        else {
            $('.js-related-question-list').html('');
            $('.js-related-question-list').hide();
            $('.js-related-question-list').attr('disabled', true);
        }
    });
    /*--/Handle Multiple Question from Admin*/

});
var offset = 0;
/**
 * This is manage the price and calculate the total payment
 */
function paymentCalculation() {
    var price = 0;
    var js_category_list = $('.js-category-list').val().split('-');
    $(js_category_list).each(function (key, cat_id) {
        var payment_val = 0;
        var count = 1;
        if ($('#js-user-category-ck-box' + cat_id).is(':checked')) {
            /*for dental mocks and plab1 mocs category*/
            if (cat_id == 2 || cat_id == 3) {
                var count = $('#PaymentTestidsArray' + cat_id + ' option:selected').length;
                if (count > 0) {
                    payment_val = $('#PaymentAmount' + cat_id).val();
                }
            } else {
                payment_val = $('#PaymentAmount' + cat_id).val();
            }
            if (payment_val) {
                var price_val = payment_val.split('=').pop();
                price_val = parseFloat(price_val) * count;
                price = price + price_val;
                //console.log(price)
            }
        }
        $('.js-total-amount').html('£ ' + price.toFixed(2));
    });
}
/*Question handled subject wise in Test manager assign question into test*/
function questionHandle(){
    var is_selected = $('#TestsQuestionQuestionId option:selected').length;
    if(is_selected>0){
        $('.js-assign-question-into-test-btn').removeAttr('disabled');
    } else{
        $('.js-assign-question-into-test-btn').attr('disabled', true);
    }
}

/*Pagination Handle of Mock Test*/
/**
 *
 * @param offset
 */
function oneByOneQuestionHandle(offset){
    var test_id = $('#test_id').val();
    var total_question = $('#total_question').val();
    /*var current_page = offset + 1;
     var current_page = offset < total_question ? current_page : total_question;*/
    $.get(ROOT + 'tests_questions/ajax_held_exam/'+test_id+'/1/'+offset+'/'+total_question, function(response){

        $('.js-question-box').html(response);
        $('#n_of_n').html($('#current-page').val());    // change current page
        var question_id = $('#question-id').val();
        $('#question-id-button').val(question_id);      // change question id button val
        // change flag
        if($('#red-flag').val().length) {
            $('#question-id-button').find('.js-flag').addClass('red-flag');
        } else{
            $('#question-id-button').find('.js-flag').removeClass('red-flag');
        }
    });
}
/**
 * Question Bank Handle
 * @param offset
 */
function oneByOneQuestionBankHandle(offset){
    //console.log('R', offset)
    var test_id = $('#test_id').val();
    var total_question = $('#total_question').val();
    var review = $('#review').val();
    var test_type = $('#js-test-type').val();
    $.get(ROOT + 'tests_questions/ajax_question_bank_practice/'+test_id+'/1/'+offset+'/'+total_question+'/'+review+'/'+test_type, function(response){
        $('.js-exam-held').html(response);
        $('html,body').animate({ scrollTop: 0 },400);
    });
}
/**
 * This works for mock test
 * @param offset
 */
function submitAnswer(offset){
    //console.log(offset)
    var test_id = $('#test_id').val();
    var total_question = $('#total_question').val();
    var test_type = $('#js-test-type').val();

    if(is_multiple_ans) {
        var answer_arr = [];
        var question_arr = [];
        var ans_index_arr = [];
        $.each($('.js-multi-ques-ans'), function(key, item){
            question_arr.push($(item).parent('td').find('p').attr('id'));
            answer_arr.push($(item).val());
            var _index = parseInt($(item).prop('selectedIndex')) -1;
            ans_index_arr.push(convertNumberIntoAlphabet(_index));
        });
        var answer  = answer_arr.join('#');
        var ans_index  = ans_index_arr.join('#');
        /*Get question ids*/
        var question_id = question_arr.join('#');
        var is_multiple =  true;
    }  else {
        var question_id = $('#question-id').val();
        var answer = $('.js-answer:checked').val();
        var is_multiple =  false;
        var ans_index = convertNumberIntoAlphabet($('.js-answer:checked').parent('li').index());
    }

    var data = {
        'answer' : answer,
        'current_question_id' : question_id,
        'ans_index' : ans_index,
        'test_type' : test_type,
        'is_multiple' : is_multiple
    };

    $.post(ROOT + 'tests_questions/ajax_held_exam/'+test_id+'/1/'+offset+'/'+total_question,data, function(response){
        var all_answered = parseInt($('#all-answered').val());
        if(all_answered) {
            $('.exam-box').html(response);
            var user_id = $('#user_id').val();
            var save_data = {
                'data[Saveandexit][user_id]': user_id,
                'data[Saveandexit][test_id]': test_id,
                'data[Saveandexit][exit_time]': 1
            };
            $.post(ROOT + 'saveandexits/save_and_exit',save_data, function(response){});
        } else{
            $('.js-question-box').html(response);
            /*var current_page = offset + 1;
             var current_page = offset < total_question ? current_page : total_question;*/
            $('#n_of_n').html($('#current-page').val());    // change current page
            var next_question_id = $('#question-id').val();
            $('#question-id-button').val(next_question_id);      // change question id button val

            if($('#red-flag').val().length) {
                $('#question-id-button').find('.js-flag').addClass('red-flag');
            } else{
                $('#question-id-button').find('.js-flag').removeClass('red-flag');
            }
            var _question_id = question_id.split('#')[0];
            var _ans_index = ans_index.replace(/#/g, ',');
            $('#js-ans-index-'+_question_id).html(_ans_index);        //change ans index.
        }
    });
}
/**
 * This works for Question Bank Test Exam
 * @param offset
 */
function questionBankPracticeSubmitAnswer(offset){
    var test_id = $('#test_id').val();
    var total_question = $('#total_question').val();
    if(is_multiple_ans) {
        var answer_arr = [];
        var question_arr = [];
        var ans_index_arr = [];
        $.each($('.js-multi-ques-ans'), function(key, item){
            question_arr.push($(item).parent('td').find('p').attr('id'));
            answer_arr.push($(item).val());
            var _index = parseInt($(item).prop('selectedIndex')) -1;
            ans_index_arr.push(convertNumberIntoAlphabet(_index));
        });
        var answer  = answer_arr.join('#');
        var ans_index  = ans_index_arr.join('#');
        /*Get question ids*/
        var question_id = question_arr.join('#');
        var is_multiple =  true;
    }  else {
        var question_id = $('#question-id-button').val();
        var answer = $('.js-answer:checked').val();
        var is_multiple =  false;
        var ans_index = convertNumberIntoAlphabet($('.js-answer:checked').parent('li').index());
    }
    //console.log(ans_index)
    // var question_id = $('#question-id-button').val();
    var review = $('#review').val();
    var test_type = $('#js-test-type').val();
    //console.log(test_type)
    var data = {
        'answer' : answer,
        'current_question_id' : question_id,
        'ans_index' : ans_index,
        'test_type' : test_type,
        'is_multiple' : is_multiple
    };
    $.post(ROOT + 'tests_questions/ajax_question_bank_practice/'+test_id+'/1/'+offset+'/'+total_question+'/'+review+'/'+test_type, data, function(response){
        $('#q-bank-exam-box').removeClass('col-md-10').addClass('col-md-7');
        $('#question-stats').show();
        $('.js-exam-held').html(response);
    });
}

/**
 *
 * @param number
 * @returns {string}
 */
function convertNumberIntoAlphabet(number){
    return String.fromCharCode(97 + number).toUpperCase();
}
/**
 *
 * @param length
 * @returns {string}
 */
function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}





