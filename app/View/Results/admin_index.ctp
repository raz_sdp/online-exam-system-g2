<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Users</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                        <?php echo $this->Form->create('User', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Group</label>
                                    <div class="col-sm-10">

                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Lecture name"
                                    value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->






                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('test_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('mark'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($results as $result): ?>
                                    <tr>
                                        <td><?php echo h($result['Result']['id']); ?>&nbsp;</td>
                                        <td>
                                        <?php echo $this->Html->link($result['Test']['title'], array('controller' => 'tests', 'action' => 'view', $result['Test']['id'])); ?>
                                        </td>
                                        <td>
                                        <?php echo $this->Html->link($result['User']['id'], array('controller' => 'users', 'action' => 'view', $result['User']['id'])); ?>
                                        </td>
                                        <td><?php echo h($result['Result']['mark']); ?>&nbsp;</td>
                                        <td><?php echo h($result['Result']['created']); ?>&nbsp;</td>
                                        <td><?php echo h($result['Result']['modified']); ?>&nbsp;</td>
                                        <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $result['Result']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $result['Result']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $result['Result']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $result['Result']['id']))); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>
                            <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>





















    <div class="results index">
	<h2><?php echo __('Results'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('test_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('mark'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($results as $result): ?>
	<tr>
		<td><?php echo h($result['Result']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($result['Test']['title'], array('controller' => 'tests', 'action' => 'view', $result['Test']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($result['User']['id'], array('controller' => 'users', 'action' => 'view', $result['User']['id'])); ?>
		</td>
		<td><?php echo h($result['Result']['mark']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['created']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $result['Result']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $result['Result']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $result['Result']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $result['Result']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Result'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
