<?php
//echo $this->Html->script(array('jquery.slimscroll.min'));
$page_no = $offset;
if($find == 'first') {
    $question_id = $questions['Question']['id'];
    ?>
    <input type="hidden" id="offset" value="<?php echo $offset?>">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title"><?php echo $questions['Question']['question'];?></h1>
        </div>
        <div class="panel-body">
            <ul class="list-group" id="answer-box">
                <?php
                $current_page = $page_no + 1;
                $disabled = true;
                foreach ($questions['Question']['Answer'] as $answer) {
                    /*AuthComponent::_setTrace($answer, false);
                    AuthComponent::_setTrace($answer_id, false);*/
                    if ($answer['id'] == $answer_id) {
                        $checked = "checked=checked";
                        $disabled = false;
                    } else {
                        $checked = "";
                    }
                    ?>
                    <li class="list-group-item"><input type="radio"
                                                       name="answer" class="js-answer"
                                                       value="<?php echo $answer['id']; ?>" <?php echo $checked; ?>> <?php echo $answer['answer']; ?>
                    </li>
                <?php
                }
                $disabled_text = $disabled ? "disabled=disabled" : "";
                ?>
            </ul>
            <?php
            if ($submit_btn && $disabled) {
                ?>
                <button type="button" class="btn btn-primary btn-lg btn-block" id="submit-answer-btn"
                    <?php echo $disabled_text ?>>Submit Answer & Get Marks
                </button>
                <input type="hidden" id="all-answered" value="1">
            <?php
            } else {
                ?>
                <button type="button" class="btn btn-primary btn-lg btn-block"
                        id="submit-answer-btn" <?php echo $disabled_text ?>>Answer
                </button>
                <input type="hidden" id="all-answered" value="0">
            <?php
            }
            ?>
        </div>
    </div>
<?php
} else {
    $question_id = $questions[0]['Question']['id'];
    $current_page = ($page_no + 1) . ' - ' . ($page_no + 3);
    ?>
    <input type="hidden" id="first_offset" value="<?php echo $offset ?>">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title text-justify"><?php echo !empty($questions['theme']) ? $questions['theme'] : 'Not Available' ?></h1>
        </div>
        <div class="panel-body">
            <table class="multiple-question-sec">
                <?php
                $disabled = true;
                $is_correct = false;
                unset($questions['theme']);
                foreach ($questions as $ques_key => $sin_question) {
                    $offset = $offset +1;
                    ?>
                    <tr>
                        <td style="width: 30px; vertical-align: top;color: #000000"><strong><?php echo $offset?>. </strong></td>
                        <td>
                            <p class="text-justify js-multi-ques" id="<?php echo $sin_question['Question']['id']?>"><?php echo $sin_question['Question']['question']?></p>
                            <br>
                            <select class="form-control js-multi-ques-ans">
                                <option>Please Select One</option>
                                <?php
                                $result = AppHelper::search($my_answered_question, 'question_id', $sin_question['Question']['id']);
                                foreach ($sin_question['Question']['Answer'] as $ans_key => $answer) {
                                    /*AuthComponent::_setTrace('ans_id'. $answer['id'], false);
                                    AuthComponent::_setTrace($answer_id_arr, false);*/
                                    if($result[0]['answer_id'] == $answer['id']) {
                                        $selected = 'selected="selected"';
                                        $disabled = false;
                                    } else {
                                        $selected = '';
                                    }
                                    ?>
                                    <option value="<?php echo $answer['id']; ?>" <?php echo $selected; ?>> <?php echo $answer['answer']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <br>
                        </td>
                    </tr>
                <?php
                }
                $disabled_text = $disabled ? "disabled=disabled" : "";
                ?>
            </table>
            <?php
            if ($submit_btn && $disabled) {
                ?>
                <button type="button" class="btn btn-primary btn-lg btn-block" id="submit-answer-btn"
                    <?php echo $disabled_text ?>>Submit Answer & Get Marks
                </button>
                <input type="hidden" id="all-answered" value="1">
            <?php
            } else {
                ?>
                <button type="button" class="btn btn-primary btn-lg btn-block"
                        id="submit-answer-btn" <?php echo $disabled_text ?>>Answer
                </button>
                <input type="hidden" id="all-answered" value="0">
            <?php
            }
            ?>
        </div>
    </div>
    <input type="hidden" id="offset" value="<?php echo $offset-1 ?>">
<?php
}
?>
<!--Hidden field zone-->
<input type="hidden" id="test_id" value="<?php echo $test_id ?>">
<input type="hidden" id="total_question" value="<?php echo $total_question ?>">

<!--<input type="hidden" id="review" value="--><?php //echo !empty($this->params->pass[3])  ? 1 : 0 ?><!--">-->
<input type="hidden" id="js-test-type" value="mock_test">
<input type="hidden" id="question-id" value="<?php echo $question_id ?>">
<input type="hidden" id="red-flag" value="<?php echo $red_flag ?>">
<input type="hidden" id="current-page" value="<?php echo $current_page ?>">

<script type="text/javascript">
    $('.exam-box-child-2').slimScroll({
        height: $('.exam-box-child-1').height() - 17 + 'px'
    });
</script>
