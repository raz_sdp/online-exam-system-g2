<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Add Test</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->


                <?php echo $this->Form->create('Test',array('type'=>'file'));?>
                    <div class="box-body">
                        <div class="form-group">

                        <?php  echo $this->Form->input('subject_id',array('class'=>'form-control','placeholder'=>'Subject'));?>

                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('group_id',array('class'=>'form-control','placeholder'=>'Group'));?>

                        </div>
                        <div class="form-group">
                        <?php  echo $this->Form->input('title',array('class'=>'form-control','placeholder'=>'Title'));?>
                        </div>
                        <div class="form-group">
                        <?php  echo $this->Form->input('instructions',array('class'=>'form-control','placeholder'=>'Instraction'));?>
                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('test_time',array('class'=>'form-control','placeholder'=>'Test Time'));?>

                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('status',array('class'=>'form-control','placeholder'=>'Status'));?>

                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('start_date',array('class'=>'form-control','placeholder'=>'Start Date'));?>

                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('Question',array('class'=>'form-control','placeholder'=>'Question'));?>

                        </div>

                    <?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
                        <!-- </form>-->
                    </div>



                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
