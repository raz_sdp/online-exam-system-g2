<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Users</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                        <?php echo $this->Form->create('User', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Group</label>
                                    <div class="col-sm-10">

                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Lecture name"
                                    value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->






                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('subject_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('group_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('title'); ?></th>
                                        <th><?php echo $this->Paginator->sort('instructions'); ?></th>
                                        <th><?php echo $this->Paginator->sort('test_time'); ?></th>
                                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                                        <th><?php echo $this->Paginator->sort('start_date'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($tests as $test): ?>
                                    <tr>
                                        <td><?php echo h($test['Test']['id']); ?>&nbsp;</td>
                                        <td>
                                        <?php echo $this->Html->link($test['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $test['Subject']['id'])); ?>
                                        </td>
                                        <td>
                                        <?php echo $this->Html->link($test['Group']['title'], array('controller' => 'groups', 'action' => 'view', $test['Group']['id'])); ?>
                                        </td>
                                        <td><?php echo h($test['Test']['title']); ?>&nbsp;</td>
                                        <td><?php echo h($test['Test']['instructions']); ?>&nbsp;</td>
                                        <td><?php echo h($test['Test']['test_time']); ?>&nbsp;</td>
                                        <td><?php echo h($test['Test']['status']); ?>&nbsp;</td>
                                        <td><?php echo h($test['Test']['start_date']); ?>&nbsp;</td>
                                        <td><?php echo h($test['Test']['created']); ?>&nbsp;</td>
                                        <td><?php echo h($test['Test']['modified']); ?>&nbsp;</td>
                                        <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $test['Test']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $test['Test']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $test['Test']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $test['Test']['id']))); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>
                            <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>






















<div class="tests index">
	<h2><?php echo __('Tests'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('subject_id'); ?></th>
			<th><?php echo $this->Paginator->sort('group_id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('instructions'); ?></th>
			<th><?php echo $this->Paginator->sort('test_time'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($tests as $test): ?>
	<tr>
		<td><?php echo h($test['Test']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($test['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $test['Subject']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($test['Group']['title'], array('controller' => 'groups', 'action' => 'view', $test['Group']['id'])); ?>
		</td>
		<td><?php echo h($test['Test']['title']); ?>&nbsp;</td>
		<td><?php echo h($test['Test']['instructions']); ?>&nbsp;</td>
		<td><?php echo h($test['Test']['test_time']); ?>&nbsp;</td>
		<td><?php echo h($test['Test']['status']); ?>&nbsp;</td>
		<td><?php echo h($test['Test']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($test['Test']['created']); ?>&nbsp;</td>
		<td><?php echo h($test['Test']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $test['Test']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $test['Test']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $test['Test']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $test['Test']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Test'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
