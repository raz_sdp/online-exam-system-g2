<?php
$default_no_of_ans = 4;
$test_id = @$this->params['pass'][0];
?>


    <div class="wrapper" xmlns="http://www.w3.org/1999/html">
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo __('Admin Add Question'); ?></h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                            <?php echo $this->Form->create('Question'); ?>
                                <div class="form-group">
                                <?php
                                echo $this->Form->input('subject_id', ['class' => 'form-control']);?>
                                </div>
                                <div class="form-group">
                                <?php //echo $this->Form->input('question', ['id' => 'editor', 'escape' => false]);?>
                                    <label>Question</label>
                                    <textarea cols="80" id="editor" name="data[Question][question]" rows="10"></textarea>
                                </div>
                                <!--<div class="form-group">
                                    <label for="inputEmail3">No of answers</label>
                                    <?php /*echo $this->Form->input('no_of_ans', array('label' => false, 'class' => 'form-control', 'options' => $no_of_questions, 'value' => $default_no_of_ans)); */?>
                                </div>-->


                                <div class="form-group">
                                    <!--                                <textarea id="option-1" name="data[''][''][]"></textarea>-->
                                    <div class="js-ans-input-box">
                                    <?php
                                    for ($count = 1; $count <= $default_no_of_ans; $count++) {
                                        ?>

                                        <div class="form-group">
                                            <label>Choice <?php echo $count ?></label><br>

                                            <div class="col-md-10 row">
                                                <!--                                                --><?php //echo $this->Form->input('Answer.answer.' . $count, array('label' => false, 'type'=> 'textarea','id' => 'option-'.$count, 'escape' => false)); ?>
                                                <textarea id="option-<?php echo $count; ?>" name="data[Answer][answer][]"
                                                cols="80" rows="10"></textarea>
                                            </div>
                                            <div class="col-md-2">
                                                <!--<input type="checkbox" name="data[Answer][is_correct][<?php /*echo $count */ ?>]" class=""
                                                       id="AnswerIsCorrect">
                                                Accept as Answer-->
                                                <div class="form-group">
                                                    <label>
                                                        <input type="radio" value="<?php echo $count; ?>" id="AnswerIsCorrect"
                                                        name="data[Answer][is_correct]"
                                                        class="flat-red" <?php echo $count == 1 ? 'checked' : '' ?>>
                                                        Accept as Answer
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Answer Description</label>
                                <textarea cols="80" id="editor1" name="data[Question][answer_description]" rows="10"></textarea>
                            </div>
                           <!-- --><?php
/*                            echo $this->Form->input('Test.test', ['class' => 'form-control', 'multiple' => 'multiple']);
                            */?>
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">Save</button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>

<script>
    /*no of question change*/

    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('option-1');
    CKEDITOR.replace('option-2');
    CKEDITOR.replace('option-3');
    CKEDITOR.replace('option-4');
    CKEDITOR.replace('option-5');
</script>





