<div class="users view">
    <h2><?php echo __('User'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
        <?php echo h($user['User']['id']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Fullname'); ?></dt>
        <dd>
        <?php echo h($user['User']['fullname']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Email'); ?></dt>
        <dd>
        <?php echo h($user['User']['email']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Password'); ?></dt>
        <dd>
        <?php echo h($user['User']['password']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('College'); ?></dt>
        <dd>
        <?php echo h($user['User']['college']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Address'); ?></dt>
        <dd>
        <?php echo h($user['User']['address']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('City'); ?></dt>
        <dd>
        <?php echo h($user['User']['city']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Passing Year'); ?></dt>
        <dd>
        <?php echo h($user['User']['passing_year']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Mobile'); ?></dt>
        <dd>
        <?php echo h($user['User']['mobile']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Pic'); ?></dt>
        <dd>
        <?php echo h($user['User']['pic']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Group'); ?></dt>
        <dd>
        <?php echo h($user['User']['group']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
        <?php echo h($user['User']['status']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Created'); ?></dt>
        <dd>
        <?php echo h($user['User']['created']); ?>
        &nbsp;
        </dd>
        <dt><?php echo __('Modified'); ?></dt>
        <dd>
        <?php echo h($user['User']['modified']); ?>
        &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
    <li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
    <li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
    <li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
    <li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
    <li><?php echo $this->Html->link(__('List Payments'), array('controller' => 'payments', 'action' => 'index')); ?> </li>
    <li><?php echo $this->Html->link(__('New Payment'), array('controller' => 'payments', 'action' => 'add')); ?> </li>
    <li><?php echo $this->Html->link(__('List Answers'), array('controller' => 'answers', 'action' => 'index')); ?> </li>
    <li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
    </ul>
</div>
    <div class="related">
        <h3><?php echo __('Related Payments'); ?></h3>
    <?php if (!empty($user['Payment'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <th><?php echo __('User Id'); ?></th>
                <th><?php echo __('Category Id'); ?></th>
                <th><?php echo __('Testids'); ?></th>
                <th><?php echo __('Amount'); ?></th>
                <th><?php echo __('Expiry Date'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <th><?php echo __('Modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        <?php foreach ($user['Payment'] as $payment): ?>
            <tr>
                <td><?php echo $payment['id']; ?></td>
                <td><?php echo $payment['user_id']; ?></td>
                <td><?php echo $payment['category_id']; ?></td>
                <td><?php echo $payment['testids']; ?></td>
                <td><?php echo $payment['amount']; ?></td>
                <td><?php echo $payment['expiry_date']; ?></td>
                <td><?php echo $payment['created']; ?></td>
                <td><?php echo $payment['modified']; ?></td>
                <td class="actions">
                <?php echo $this->Html->link(__('View'), array('controller' => 'payments', 'action' => 'view', $payment['id'])); ?>
                <?php echo $this->Html->link(__('Edit'), array('controller' => 'payments', 'action' => 'edit', $payment['id'])); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'payments', 'action' => 'delete', $payment['id']), array(), __('Are you sure you want to delete # %s?', $payment['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
    <?php endif; ?>

        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__('New Payment'), array('controller' => 'payments', 'action' => 'add')); ?> </li>
            </ul>
        </div>
    </div>
    <div class="related">
        <h3><?php echo __('Related Answers'); ?></h3>
    <?php if (!empty($user['Answer'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <th><?php echo __('Question Id'); ?></th>
                <th><?php echo __('Answer'); ?></th>
                <th><?php echo __('Feedback'); ?></th>
                <th><?php echo __('Is Correct'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <th><?php echo __('Modified'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        <?php foreach ($user['Answer'] as $answer): ?>
            <tr>
                <td><?php echo $answer['id']; ?></td>
                <td><?php echo $answer['question_id']; ?></td>
                <td><?php echo $answer['answer']; ?></td>
                <td><?php echo $answer['feedback']; ?></td>
                <td><?php echo $answer['is_correct']; ?></td>
                <td><?php echo $answer['created']; ?></td>
                <td><?php echo $answer['modified']; ?></td>
                <td class="actions">
                <?php echo $this->Html->link(__('View'), array('controller' => 'answers', 'action' => 'view', $answer['id'])); ?>
                <?php echo $this->Html->link(__('Edit'), array('controller' => 'answers', 'action' => 'edit', $answer['id'])); ?>
                <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'answers', 'action' => 'delete', $answer['id']), array(), __('Are you sure you want to delete # %s?', $answer['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
    <?php endif; ?>

        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__('New Answer'), array('controller' => 'answers', 'action' => 'add')); ?> </li>
            </ul>a
        </div>
    </div>
