<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Add User</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->
                <?php echo $this->Form->create('User',array('type'=>'file'));?>
                    <div class="box-body">
                        <div class="form-group">

                        <?php  echo $this->Form->input('username',array('class'=>'form-control','placeholder'=>'Username'));?>

                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Email'));?>

                        </div>
                        <div class="form-group">
                        <?php  echo $this->Form->input('role',array('class'=>'form-control','placeholder'=>'Role'));?>
                        </div>
                        <div class="form-group">
                        <?php  echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>'Password'));?>
                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('phone',array('class'=>'form-control','placeholder'=>'Mobile'));?>

                        </div>

                    <?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
                        <!-- </form>-->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


