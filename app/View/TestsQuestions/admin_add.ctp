<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Add TestsQuestion</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->


                <?php echo $this->Form->create('Test',array('type'=>'file'));?>
                    <div class="box-body">
                        <div class="form-group">

                        <?php  echo $this->Form->input('test_id',array('class'=>'form-control','placeholder'=>'Test'));?>

                        </div>
                        <div class="form-group">

                        <?php  echo $this->Form->input('question_id',array('class'=>'form-control','placeholder'=>'Question'));?>

                        </div>
                    <?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
                        <!-- </form>-->
                    </div>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
