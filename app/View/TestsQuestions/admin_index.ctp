<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Test Question</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">
                        <div class="row">
                        <?php echo $this->Form->create('User', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-9">
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Lecture name"
                                    value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->






                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('test_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('question_id'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created'); ?></th>
                                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($testsQuestions as $testsQuestion): ?>
                                    <tr>
                                        <td><?php echo h($testsQuestion['TestsQuestion']['id']); ?>&nbsp;</td>
                                        <td>
                                        <?php echo $this->Html->link($testsQuestion['Test']['title'], array('controller' => 'tests', 'action' => 'view', $testsQuestion['Test']['id'])); ?>
                                        </td>
                                        <td>
                                        <?php echo $this->Html->link($testsQuestion['Question']['id'], array('controller' => 'questions', 'action' => 'view', $testsQuestion['Question']['id'])); ?>
                                        </td>
                                        <td><?php echo h($testsQuestion['TestsQuestion']['created']); ?>&nbsp;</td>
                                        <td><?php echo h($testsQuestion['TestsQuestion']['modified']); ?>&nbsp;</td>
                                        <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $testsQuestion['TestsQuestion']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $testsQuestion['TestsQuestion']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $testsQuestion['TestsQuestion']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $testsQuestion['TestsQuestion']['id']))); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>
                            <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
