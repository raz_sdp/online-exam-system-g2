<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
    public static function search($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, self::search($subarray, $key, $value));
            }
        }

        return $results;
    }

    public static function dashBoardQuestionSearch($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            $array = Hash::extract($array, '{n}.Question');
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, self::search($subarray, $key, $value));
            }
        }

        return $results;
    }

    public static function getPriceLists($data){
        $price_lists = array();
        if(!empty($data)) {
            foreach($data as $item){
                $price_lists[$item['month']."=".$item['price']] = $item['month'].' Month £'.$item['price'];
            }
        } else{
            $price_lists[''] = 'Price Not Found';
        }
        return $price_lists;
    }
    /**
     * @param $number
     * @return mixed
     */
    public static function convertNumberToAlphabetLetter($number)
    {
        $alphabet = range('A', 'Z');
        return $alphabet[$number];      # For 0 show A
    }
}
