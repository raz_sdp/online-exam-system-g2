<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public function getNumberList($max_number = 0){
        $number_lists = [];
        for($init = 1; $init<=$max_number; $init++) {
            $number_lists[$init] = $init;
        }
        return $number_lists;
    }

    /*public $components = array(
'Session', 'RequestHandler','Cookie',
'Auth' => array(
    /*'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
    'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'admin' => true),
    'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
    'authError' => 'You are not allowed',
    'authenticate' => array(
        'Form' => array(
            'fields' => array('username' => 'email', 'password' => 'password')
        )
    )
)
);*/

   /* public function beforeFilter(){
        $this->Auth->allow(
            'login','logout', 'mock_test','held_exam','main','notes','note_edit','note_view',
            'get_tests_categorise','guest','register','save_and_exit', 'register',
            'get_choice_lists', 'create_question_select_box', 'add_rating','my_account','create_selectbox_for_related_question',
            'question_bank','q_bank_practice','ajax_held_exam','ajax_question_bank_practice','users_answer','add',
            'check_my_mock_test_status','paypal_success','paypal_payment','validate_input','paypal_cancel','make_login_false',
            'calculate_limit_offset','update_price'
        );

        if(!$this->params['admin']) {
            $this->layout = 'front';
        }
    }
    public function get_user_id(){
        $login_user = $this->Session->read('user_info');
        return $login_user['id'];
    }*/
}
