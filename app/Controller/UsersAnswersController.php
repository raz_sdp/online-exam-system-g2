<?php
App::uses('AppController', 'Controller');
/**
 * UsersAnswers Controller
 *
 * @property UsersAnswer $UsersAnswer
 * @property PaginatorComponent $Paginator
 */
class UsersAnswersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UsersAnswer->recursive = 0;
		$this->set('usersAnswers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UsersAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		$options = array('conditions' => array('UsersAnswer.' . $this->UsersAnswer->primaryKey => $id));
		$this->set('usersAnswer', $this->UsersAnswer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UsersAnswer->create();
			if ($this->UsersAnswer->save($this->request->data)) {
				$this->Flash->success(__('The users answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users answer could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersAnswer->User->find('list');
		$tests = $this->UsersAnswer->Test->find('list');
		$questions = $this->UsersAnswer->Question->find('list');
		$answers = $this->UsersAnswer->Answer->find('list');
		$this->set(compact('users', 'tests', 'questions', 'answers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UsersAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersAnswer->save($this->request->data)) {
				$this->Flash->success(__('The users answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users answer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersAnswer.' . $this->UsersAnswer->primaryKey => $id));
			$this->request->data = $this->UsersAnswer->find('first', $options);
		}
		$users = $this->UsersAnswer->User->find('list');
		$tests = $this->UsersAnswer->Test->find('list');
		$questions = $this->UsersAnswer->Question->find('list');
		$answers = $this->UsersAnswer->Answer->find('list');
		$this->set(compact('users', 'tests', 'questions', 'answers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UsersAnswer->id = $id;
		if (!$this->UsersAnswer->exists()) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersAnswer->delete()) {
			$this->Flash->success(__('The users answer has been deleted.'));
		} else {
			$this->Flash->error(__('The users answer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UsersAnswer->recursive = 0;
		$this->set('usersAnswers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UsersAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		$options = array('conditions' => array('UsersAnswer.' . $this->UsersAnswer->primaryKey => $id));
		$this->set('usersAnswer', $this->UsersAnswer->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UsersAnswer->create();
			if ($this->UsersAnswer->save($this->request->data)) {
				$this->Flash->success(__('The users answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users answer could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersAnswer->User->find('list');
		$tests = $this->UsersAnswer->Test->find('list');
		$questions = $this->UsersAnswer->Question->find('list');
		$answers = $this->UsersAnswer->Answer->find('list');
		$this->set(compact('users', 'tests', 'questions', 'answers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UsersAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UsersAnswer->save($this->request->data)) {
				$this->Flash->success(__('The users answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The users answer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersAnswer.' . $this->UsersAnswer->primaryKey => $id));
			$this->request->data = $this->UsersAnswer->find('first', $options);
		}
		$users = $this->UsersAnswer->User->find('list');
		$tests = $this->UsersAnswer->Test->find('list');
		$questions = $this->UsersAnswer->Question->find('list');
		$answers = $this->UsersAnswer->Answer->find('list');
		$this->set(compact('users', 'tests', 'questions', 'answers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UsersAnswer->id = $id;
		if (!$this->UsersAnswer->exists()) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersAnswer->delete()) {
			$this->Flash->success(__('The users answer has been deleted.'));
		} else {
			$this->Flash->error(__('The users answer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


    public function users_answer($user_id = null, $is_flag = 0, $test_id=null, $question_id = null, $test_type= 'mock_test'){
        $this->autoLayout = false;
        $users_answer_data['UsersAnswer'] = [
            'user_id' => $user_id,
            'test_id' => $test_id,
            'question_id' => $question_id,
            'is_flag' => $is_flag,
            'test_type' => $test_type,
        ];
        $is_answered = $this->_is_answered($test_id, $question_id, $user_id);
        if(!empty($is_answered)){
            $this-> UsersAnswer-> id = $is_answered['UsersAnswer']['id'];
        } else {
            $this-> UsersAnswer-> create();
        }
        if($this->UsersAnswer->save($users_answer_data)) {
            die(json_encode(array('status' => true)));
        } else{
            die(json_encode(array('status' => false)));
        }
    }

    public function answer_submit($test_id=null, $question_id = null, $answer_id = null, $user_id = null, $ans_index = null, $test_type = null){
        $this->autoLayout = false;
        $users_answer_data['UsersAnswer'] = [
            'user_id' => $user_id,
            'test_id' => $test_id,
            'question_id' => $question_id,
            'answer_id' => $answer_id,
            'ans_index' => $ans_index,
            'test_type' => $test_type,
        ];
        $is_answered = $this->_is_answered($test_id, $question_id, $user_id);
        if(!empty($is_answered)){
            $this-> UsersAnswer-> id = $is_answered['UsersAnswer']['id'];
            if($this-> UsersAnswer-> save($users_answer_data)){
                $success = true;
            } else{
                $success = false;
            }
        } else {
            $this-> UsersAnswer-> create();
            if($this-> UsersAnswer->save($users_answer_data)){
                $success = true;
            } else{
                $success = false;
            }
        }
        return $success;
    }

    private function _is_answered($test_id = null, $question_id = null, $user_id = null){
        $query = [
            'recursive' => -1,
            'field' => ['UsersAnswer.is_flag'],
            'conditions' => [
                'UsersAnswer.user_id' => $user_id,
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.question_id' => $question_id,
            ],
        ];
        return $this-> UsersAnswer-> find('first',$query);
    }

    /**
     * @param null $test_id
     * @param null $user_id
     * @return array|null
     * get all question at the end of the exam for showing Result table
     */
    public function get_all_questions_after_exam($test_id = null, $user_id = null, $test_type = null){
        $this-> UsersAnswer->Behaviors->load('Containable');
        $query = [
            'conditions' => [
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.user_id' => $user_id,
                'UsersAnswer.test_type' => $test_type,
                //'UsersAnswer.ans_index IS NOT NULL'
            ],
            'fields' => ['UsersAnswer.question_id', 'UsersAnswer.answer_id','UsersAnswer.is_flag','UsersAnswer.ans_index'],
            'contain' => [
                'Question' => [
                    'fields' => ['Question.id','Question.question'],
                    'Answer' => [
                        'fields' => ['Answer.id'],
                        'conditions' => ['Answer.is_correct'=> 1],
                    ],
                ],
                'Test' => [
                    'fields' => ['Test.id','Test.title'],
                    'conditions' => ['Test.status' => 1]
                ],
            ],
        ];
        return $this->UsersAnswer->find('all', $query);
    }

    /**
     * @param null $test_id
     * @param null $user_id
     * @return array|null
     * My answered question
     */
    public  function no_of_answered_question($test_id = null, $user_id = null, $test_type = ''){

        $query = [
            'recursive' => -1,
            'conditions' => [
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.user_id' => $user_id,
                'UsersAnswer.test_type' => $test_type,
                'UsersAnswer.answer_id IS NOT NULL',
            ],
        ];
        $my_answered_question = $this->UsersAnswer->find('all', $query);
        return $my_answered_question;
    }

    /**
     * @param null $test_id
     * @param null $question_id
     * @param null $answer_of
     * @param null $total_no_user
     * @return float
     * Return percentage amount of answer for A or B or C and so on...
     */
    public function calculateQuestionStats($test_id = null, $question_id = null, $answer_of = null, $total_no_user = 0, $test_type){
        $query = [
            'recursive' => -1,
            'conditions' => [
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.question_id' => $question_id,
                'UsersAnswer.test_type' => $test_type,
                'UsersAnswer.ans_index' => $answer_of,
            ],
        ];
        $no_of_user = $this->UsersAnswer-> find('count', $query);
        $result = 0;
        if(!empty($total_no_user)) {
            $result = round(($no_of_user/$total_no_user)*100);
        }
        return $result;
    }

    /**
     * @param null $test_id
     * @param null $question_id
     * @param null $answer_of
     * @param int $total_no_user
     * @param $test_type
     * @return float|int
     */
    public function calculateMultipleQuestionStats($test_id = null, $question_id = null, $correct_answer = null,  $total_no_user = 0, $test_type){
        $query = [
            'recursive' => -1,
            'conditions' => [
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.question_id' => $question_id,
                'UsersAnswer.test_type' => $test_type,
                'UsersAnswer.answer_id' => $correct_answer,
            ],
        ];
        $no_of_user = $this->UsersAnswer-> find('count', $query);       //Correct Answer User number
        $result = 0;
        if(!empty($total_no_user)) {
            $result = round(($no_of_user/$total_no_user)*100);
        }
        return $result;
    }


    public function totalAnsweredOfThisQuestion($test_id = null, $question_id = null, $test_type){
        $query = [
            'recursive' => -1,
            'conditions' => [
                'UsersAnswer.test_id' => $test_id,
                'UsersAnswer.question_id' => $question_id,
                'UsersAnswer.test_type' => $test_type,
            ],
        ];
        return $this->UsersAnswer-> find('count', $query);
    }
}
