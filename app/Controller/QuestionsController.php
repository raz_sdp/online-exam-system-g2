<?php
App::uses('AppController', 'Controller');
/**
 * Questions Controller
 *
 * @property Question $Question
 * @property PaginatorComponent $Paginator
 */
class QuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Question->recursive = 0;
		$this->set('questions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
		$this->set('question', $this->Question->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Question->create();
			if ($this->Question->save($this->request->data)) {
				$this->Flash->success(__('The question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The question could not be saved. Please, try again.'));
			}
		}
		$subjects = $this->Question->Subject->find('list');
		$tests = $this->Question->Test->find('list');
		$this->set(compact('subjects', 'tests'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Question->save($this->request->data)) {
				$this->Flash->success(__('The question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
			$this->request->data = $this->Question->find('first', $options);
		}
		$subjects = $this->Question->Subject->find('list');
		$tests = $this->Question->Test->find('list');
		$this->set(compact('subjects', 'tests'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Question->delete()) {
			$this->Flash->success(__('The question has been deleted.'));
		} else {
			$this->Flash->error(__('The question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Question->recursive = 0;
		$this->set('questions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
		$this->set('question', $this->Question->find('first', $options));
	}

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->request->data['Question']['question'] = strip_tags($this->request->data['Question']['question'],'<sub>,<sup>');
            $this->request->data['Question']['answer_description'] = strip_tags($this->request->data['Question']['answer_description'],'<sub>,<sup>');
            //print_r($this->request->data);die;
            $this->Question->create();
            if ($this->Question->save($this->request->data)) {
                $answers = $this->request->data['Answer'];
                if(!empty($answers)){
                    foreach($answers['answer'] as $key => $item){
                        $answer_data['Answer'] = array(
                            'question_id' => $this->Question->id,
                            'answer' => strip_tags($item,'<sub>,<sup>'),
                            'feedback' => $answers['feedback'][$key],
                            'is_correct' => $answers['is_correct'],
                        );
                        $this->Question->Answer->create();
                        $this->Question->Answer->save($answer_data);
                    }
                }

                $this->Session->setFlash(__('The question has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The question could not be saved. Please, try again.'));
            }
        }
        $subjects = $this->Question->Subject->find('list');
        $tests = $this->Question->Test->find('list');

        $no_of_questions = $this->getNumberList(10);
        $this->set(compact('subjects', 'tests','no_of_questions'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Question->exists($id)) {
            throw new NotFoundException(__('Invalid question'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Question']['question'] = strip_tags($this->request->data['Question']['question'],'<sub>,<sup>');
            $this->request->data['Question']['answer_description'] = strip_tags($this->request->data['Question']['answer_description'],'<sub>,<sup>');
            if ($this->Question->save($this->request->data)) {
                $this->Question->Answer->deleteAll(['Answer.question_id'=> $id], false);
                //pr($this->request->data);die;
                $answers = $this->request->data['Answer'];
                if(!empty($answers)){
                    foreach($answers['answer'] as $key => $item){
                        $answer_data['Answer'] = array(
                            'question_id' => $this->Question->id,
                            'answer' => strip_tags($item,'<sub>,<sup>'),
                            'feedback' => $answers['feedback'][$key],
                            'is_correct' => ++$key==$answers['is_correct'] ? 1 : null,
                        );
                        $this->Question->Answer->create();
                        $this->Question->id = $id;
                        $this->Question->Answer->save($answer_data);
                    }
                }
                $this->Session->setFlash(__('The question has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The question could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
            $this->request->data = $this->Question->find('first', $options);
        }
        $query = array(
            'recursive' => -1,
            'conditions' => array(
                'Answer.question_id' => $id,
            ),
        );
        #$answers = $this->Question->Answer->find('all', $query);

        $subjects = $this->Question->Subject->find('list');
        $tests = $this->Question->Test->find('list');
        $no_of_questions = $this->getNumberList(10);
        $this->set(compact('subjects', 'tests','no_of_questions'));
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Question->delete()) {
			$this->Flash->success(__('The question has been deleted.'));
		} else {
			$this->Flash->error(__('The question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
